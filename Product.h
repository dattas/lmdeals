//
//  Product.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@interface Product : NSObject
{
    NSString *productImgLink;
    NSString *productName;
    NSString *productId;
    NSString *productDesciption;
    NSString *productOffer;
    NSString *productText;
    NSString *termsAndConditions;
    UIImage *productImage;
    BOOL imageDoesntExist;
    
    // Contact Properties.
    NSString *contactMerchantName;
    NSString *contactFirstName;
    NSString *contactlastName;
    NSString *contactTitle;
    NSString *contactAddress1;
    NSString *contactAddress2;

    NSString *phone;
    NSString *mobile;
    NSString *websiteLink;
    
    NSString *contactSuburb;
    NSString *contactState;
    NSString *contactPostcode;
    NSString *contactCountry;
    
    NSString *displayImagePrefix;
    NSString *imageExtension;
    NSString *pin_type;
    NSString *hotoffer_extension;
    
    // Other properties
    NSString *productDetailedDesciption;
    NSString *productTermsAndConditions;
    CLLocationCoordinate2D coordinate;
    
    
    
    //image properties
    NSString *merchantId;
    NSString *merchantLogoExtention;
    
    
    //post a deal properties
    int product_AvailableCount;
    NSString *product_CategoryId;
    int product_HoursCountDown;
    NSString *product_StartDate;
    NSString *product_EndDate;
    int product_FbStatus;
    NSString *product_FbUrl;
    int product_TwitterStatus;
    NSString *product_TwitterUrl;
    int product_EmailStatus;
    NSString *product_EmailAddress;
    int product_PhoneNoStatus;
    NSString *product_PhoneNo1;
    NSString *product_PhoneNo2;
    int product_BookNowStatus;
    NSString *product_BookNowText;
    int product_WebsiteStatus;
    NSString *product_WebsiteUrl;
    int product_CommentStatus;
    NSString *product_CommentText;
    int product_ScoreStatus;
    NSString *product_ScoreText;
    int product_ScanStatus;
    NSString *product_ScanText;
    int product_RedeemStatus;
    NSString *product_RedeemText;
    int product_Active;
    int StoreId;
    NSString *categoryId;
    
}

@property (nonatomic, strong) NSString *productImgLink;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *productDesciption;
@property (nonatomic, strong) NSString *productOffer;
@property (nonatomic, strong) NSString *productText;
@property (nonatomic, strong) NSString *termsAndConditions;
@property (nonatomic, strong) UIImage *productImage;

// This property exactly is not Product's requirement.
// This is for image presenting logic.
@property (nonatomic, readwrite) BOOL imageDoesntExist;


@property (nonatomic, strong) NSString *productDetailedDesciption;
@property (nonatomic, strong) NSString *productTermsAndConditions;

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *websiteLink;
@property (nonatomic, strong) NSString *pin_type;
@property (nonatomic, strong) NSString *hotoffer_extension;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;


@property (nonatomic, strong) NSString *contactMerchantName;
@property (nonatomic, strong) NSString *contactFirstName;
@property (nonatomic, strong) NSString *contactlastName;
@property (nonatomic, strong) NSString *contactSuburb;
@property (nonatomic, strong) NSString *contactState;
@property (nonatomic, strong) NSString *contactPostcode;
@property (nonatomic, strong) NSString *contactCountry;

@property (nonatomic, strong) NSString *displayImagePrefix;
@property (nonatomic, strong) NSString *imageExtension;

@property (nonatomic, strong) NSString *merchantId;
@property (nonatomic, strong) NSString *merchantLogoExtention;

//post a deal properties
@property (nonatomic, readwrite)int product_AvailableCount;
@property (nonatomic, strong)NSString *product_CategoryId;
@property (nonatomic, readwrite)int product_HoursCountDown;
@property (nonatomic, strong)NSString *product_StartDate;
@property (nonatomic, strong)NSString *product_EndDate;
@property (nonatomic, readwrite)int product_FbStatus;
@property (nonatomic, strong)NSString *product_FbUrl;
@property (nonatomic, readwrite)int product_TwitterStatus;
@property (nonatomic, strong)NSString *product_TwitterUrl;
@property (nonatomic, readwrite)int product_EmailStatus;
@property (nonatomic, strong)NSString *product_EmailAddress;
@property (nonatomic, readwrite)int product_PhoneNoStatus;
@property (nonatomic, strong)NSString *product_PhoneNo1;
@property (nonatomic, strong)NSString *product_PhoneNo2;
@property (nonatomic, readwrite)int product_BookNowStatus;
@property (nonatomic, strong)NSString *product_BookNowText;
@property (nonatomic, readwrite)int product_WebsiteStatus;
@property (nonatomic, strong)NSString *product_WebsiteUrl;
@property (nonatomic, readwrite)int product_CommentStatus;
@property (nonatomic, strong)NSString *product_CommentText;
@property (nonatomic, readwrite)int product_ScoreStatus;
@property (nonatomic, strong)NSString *product_ScoreText;
@property (nonatomic, readwrite)int product_ScanStatus;
@property (nonatomic, strong)NSString *product_ScanText;
@property (nonatomic, readwrite)int product_RedeemStatus;
@property (nonatomic, strong)NSString *product_RedeemText;
@property (nonatomic, readwrite)int product_Active;
@property (nonatomic, readwrite)int StoreId;
@property (nonatomic, strong) NSString *categoryId;
@end










