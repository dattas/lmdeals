//
//  PostADealCell.h
//  MyARSample
//
//  Created by vairat on 04/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostADealCell : UITableViewCell

@property(nonatomic, retain)IBOutlet UIButton *cellButton;
@property(nonatomic, retain)IBOutlet UILabel *cellLabel;
@property(nonatomic, retain)IBOutlet UISwitch *cellSwitch;

@end
