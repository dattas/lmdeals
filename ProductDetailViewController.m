//
//  ProductDetailViewController.m
//  MyARSample
//
//  Created by vairat on 04/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "PostADealViewController.h"
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import "MerchantAddressCell.h"
#import "MyCustomAlert.h"
#import "AppDelegate.h"
#import "CustomIOS7AlertView.h"

@interface ProductDetailViewController (){
    
    UIView *offerView;
    Product *thisProduct;
    NSString *productID;
    
    ProductDataParser *productDataXMLParser;
    MerchantListParser *merchantListParser;
    AppDelegate *appDelegate;
    
    NSURLConnection *conn;
    NSURLConnection *proddImage;
    NSURLConnection *merchantFetchRequest;
    
    UIView *addressView;
    UIView *tcView;
    BOOL addressDetailsLoaded;
    BOOL sideMenuOpen;
    
    int direction;
    int shakes;
    
    //NSArray *segmentedControlImages_Array;
   // NSArray *segmentedControlHihglightImages_Array;
    
    CustomIOS7AlertView *alertView7;
    
    UIView *demoView;
    
    
}
@property (nonatomic, strong) UIView *offerView;
@property (nonatomic, strong) UIView *addressView;
@property (nonatomic, strong) UIView *tcView;
@property (nonatomic, strong) Product *thisProduct;
@property(nonatomic,retain)NSURLConnection *conn;
@property(nonatomic,retain)NSURLConnection *proddImage;
@property(nonatomic,retain)NSURLConnection *merchantFetchRequest;



-(void)facebookButton_Action;
-(void)twitterButton_Action;
-(void)emailButton_Action;
-(void)phoneButton_Action;
-(void)bookNowButton_Action;
-(void)websiteButton_Action;
-(void)commentButton_Action;
-(void)scorePointsButton_Action;
-(void)redeemQRButton_Action;
-(void)redeemPasswordButton_Action;
@end

@implementation ProductDetailViewController

@synthesize productNameLabel,productOfferLabel;
@synthesize segmentedControl;
@synthesize mainContentHolder;
@synthesize offerView;
@synthesize tcView;
@synthesize thisProduct;
@synthesize conn;
@synthesize merchantFetchRequest;
@synthesize imageView;
@synthesize activityView;
@synthesize containerView;
@synthesize imgContainerView;
@synthesize scroller;
@synthesize merchantTableView;
@synthesize Location;
@synthesize sideSlideView;
@synthesize redeemPin_TextField;
@synthesize favButton;
@synthesize addressView;
@synthesize proddImage;
@synthesize startDate_Label;
@synthesize endDate_Label;
@synthesize hoursCountDown_Label;
@synthesize productsLeftCount_Label;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productID:(NSString *)ProId
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         productID = ProId;
        addressDetailsLoaded = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    self.title = @"Product Details";
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Edit"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(editButtonTapped:)];
    
    sideMenuOpen = NO;
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self showActivityView];
    
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor blackColor].CGColor;
    imageView.layer.cornerRadius = 1.0;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_deal.php?id=%@",productID]]];
    //NSLog(@"",[NSURL URLWithString:[NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_deal.php?id=%@",productID])
    
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
  //  segmentedControlImages_Array = [[NSArray alloc]initWithObjects:@"Offer.png",@"T&C.png",@"Stores.png", nil];
  //  segmentedControlHihglightImages_Array = [[NSArray alloc]initWithObjects:@"Offer_Highlight.png",@"T&C_Highlight.png",@"Stores_Highlight.png", nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   [self.navigationController.navigationBar setTranslucent:NO];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    scroller.frame = self.containerView.bounds;
}

-(void)editButtonTapped:(id)sender{
    
    
    NSLog(@"editButtonTapped");
    PostADealViewController *postaDeal = [[PostADealViewController alloc]initWithNibName:@"PostADealViewController" bundle:Nil throughView:@"productDetail"];
    postaDeal.current_PostingProduct = thisProduct;
    [self.navigationController pushViewController:postaDeal animated:YES];
}





-(IBAction)sideMenuTapped_Actions:(id)sender
{
    if(sideMenuOpen)
    {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            CGRect ContainerFrame = self.sideSlideView.frame;
            ContainerFrame.origin.x = self.sideSlideView.frame.origin.x + 285.0;
            self.sideSlideView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             sideMenuOpen = NO;
                             
                         } ];
        
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.sideSlideView.frame;
            ContainerFrame.origin.x = self.sideSlideView.frame.origin.x - 285.0;
            self.sideSlideView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             sideMenuOpen = YES;
                             
                         } ];
        
        
        
        
    }
    
    
}
- (IBAction) favoritesButtonPressed:(id) sender {
    
    if (![appDelegate productExistsInFavorites:thisProduct]) {
        
        
        MyCustomAlert *alert = [[MyCustomAlert alloc] initWithTitle:@"Add to favorites?" message:[NSString stringWithFormat:@"Do you want to add %@ to favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.tag = 21;
        [alert show];
    }
    else {
        
        
        MyCustomAlert *alert = [[MyCustomAlert alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        alert.tag = 22;
        [alert show];
    }
    
}




- (IBAction) mapButtonPressed:(id) sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====");
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

-(IBAction)buttonTapped_Actions:(id)sender
{
    
    switch ([sender tag])
    {
        case FacebookButton:
                           
                             [self facebookButton_Action];
                             break;
        case TwitterButton:
                             [self twitterButton_Action];
                             break;
        case EmailButton:
                             [self emailButton_Action];
                             break;
        case PhoneButton:
                             [self phoneButton_Action];
                             break;
        case BooknowButton:
                             
                             break;
        case WebsiteButton:
                             [self websiteButton_Action];
                             break;
        case commentButton:
                           //[self presentMyUnionrewardsController];
                             NSLog(@"commentButton");
                             break;
        case ScorePountsButton:
                           //[self presentSearchMYRewardsController];
                              NSLog(@"ScorePountsButton");
                              break;
        case RedeemQRButton:
                             [self redeemQRButton_Action];
                             break;
        case RedeemPasswordButton:
            
                             [self redeemPasswordButton_Action];
                             break;
        
            
            
        default:
            break;
    }

}


-(IBAction) segmentedControlIndexChanged: (id)sender{
    
     NSLog(@"segmentedControlIndex %d",self.segmentedControl.selectedSegmentIndex);
    
    NSMutableString *tempStr;
    for( int i = 0; i < 3; i++ ) {
        
       // tempStr = [segmentedControlImages_Array objectAtIndex:i];
        NSLog(@"Image::%@",tempStr);
      //  [self.segmentedControl setImage:[UIImage imageNamed:tempStr] forSegmentAtIndex:i];
        
    }
    
    //setting image to the selected segment section.
   // tempStr = [segmentedControlHihglightImages_Array objectAtIndex:self.segmentedControl.selectedSegmentIndex];
    NSLog(@"Highlight Image::%@",tempStr);
  //  [self.segmentedControl setImage:[UIImage imageNamed:tempStr] forSegmentAtIndex:self.segmentedControl.selectedSegmentIndex];
    
    switch (self.segmentedControl.selectedSegmentIndex) {
            
        case 0:
              {
                //   [self.segmentedControl setImage:[[UIImage imageNamed:@"Offer.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:0];
                 if (!offerView) {
                    offerView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
                    offerView.backgroundColor = [UIColor whiteColor];
                
                    //offerView.layer.cornerRadius = 5.0;
                      offerView.layer.borderColor = [UIColor blackColor].CGColor;
                      offerView.layer.borderWidth = 2.0;
                      }
                       CGRect mainFrame = mainContentHolder.frame;
                       mainFrame.size = offerView.frame.size;
                       mainContentHolder.frame = mainFrame;
                      [mainContentHolder addSubview:self.offerView];
            
                      [self cleanMainController];
                      [self updateOfferView];
                      [self addOfferViewAsSubview];
                      //[self updateUIWithProductDetails:thisProduct];
               }
                    break;
            
        case 1:
              {
                 // [self.segmentedControl setImage:[[UIImage imageNamed:@"T&C.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:1];

                  
                  if (!tcView) {
                      tcView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
                      tcView.backgroundColor = [UIColor whiteColor];
                      tcView.layer.borderColor = [UIColor blackColor].CGColor;
                      tcView.layer.borderWidth = 2.0;
                  }
                  CGRect mainFrame = mainContentHolder.frame;
                  mainFrame.size = tcView.frame.size;
                  mainContentHolder.frame = mainFrame;
                  [mainContentHolder addSubview:self.tcView];
                  
                    [self cleanMainController];
                    [self updateTcView];
                    [self addTCViewAsSubview];
            
                 }
                  break;
        case 2:
            //[self.segmentedControl setImage:[[UIImage imageNamed:@"Stores.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:2];
            
            if (!addressView) {
                addressView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
                addressView.backgroundColor = [UIColor whiteColor];
                
                //addressView.layer.cornerRadius = 5.0;
                addressView.layer.borderColor = [UIColor blackColor].CGColor;
                addressView.layer.borderWidth = 2.0;
            }
            if(!merchantTableView)
            {
                merchantTableView = [[UITableView alloc]initWithFrame:mainContentHolder.bounds style:UITableViewStylePlain];
                merchantTableView.delegate = self;
                merchantTableView.dataSource = self;
                [addressView addSubview:merchantTableView];
            }
            
            [self cleanMainController];
            [self updateAddressView];
            [self addAddressViewAsSubview];
            
            
            
            break;
            
        default:
            
            break;
            
    }
    
}

//======================================
- (void) cleanMainController {
    
    for (UIView *subView in mainContentHolder.subviews) {
        [subView removeFromSuperview];
    }
    
}


- (void) updateScrollerContentSize {
    
    scroller.frame = self.containerView.bounds;
    
    float contentHeight = mainContentHolder.frame.origin.y + mainContentHolder.frame.size.height +40.0;
    if ([appDelegate isIphone5])
        scroller.contentSize = CGSizeMake(scroller.contentSize.width, contentHeight);
    else
         scroller.contentSize = CGSizeMake(scroller.contentSize.width, contentHeight+200);
    NSLog(@"** scroller contentSize: %@",NSStringFromCGSize(scroller.contentSize));
    NSLog(@"** scroller frame: %@",NSStringFromCGRect(scroller.frame));
    
    scroller.scrollEnabled = YES;
    
}


- (void) updateOfferView {
    
    if (!offerView) {
        return;
    }
    
    // Check if webview already exists
    
    UIWebView *offerWebView;
    //  offerWebView.scalesPageToFit = YES;
    
    if ((offerView.subviews.count != 0) ) {
        offerWebView = (UIWebView *)[offerView viewWithTag:2012];
        
    }
    else {
        offerWebView = [[UIWebView alloc] initWithFrame:offerView.bounds];
        
        NSLog(@"ELSE WEBVIEW");
    }
    
    NSString *webViewtext;
    if ([thisProduct.productDesciption length] <= 0 && [thisProduct.productText length] <= 0)
        webViewtext = @"";
    else
        webViewtext = [NSString stringWithFormat:@"%@%@",thisProduct.productDesciption,thisProduct.productText];
    
    NSString *myDescriptionHTML=[NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                                 "H4{ color: rgb(198,38,21) }\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "<body><H4>%@</H4>%@</body> \n"
                                 "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer, webViewtext];
    
    [offerWebView loadHTMLString:myDescriptionHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    offerWebView.tag = 2012;
    offerWebView.delegate = self;
    [offerView addSubview:offerWebView];
    
}
-(void)updateTcView {
    
    if (!tcView) {
        return;
    }
    
    // Check if webview already exists
    
    UIWebView *tcWebView;
    //  offerWebView.scalesPageToFit = YES;
    
    if ((tcView.subviews.count != 0) ) {
        tcWebView = (UIWebView *)[tcView viewWithTag:2013];
        
    }
    else {
        tcWebView = [[UIWebView alloc] initWithFrame:offerView.bounds];
        
        NSLog(@"ELSE WEBVIEW");
    }
    
    NSString *webViewtext;
    if ([thisProduct.termsAndConditions length] <= 0)
        webViewtext = @"No Terms & Conditions";
    else
        webViewtext = [NSString stringWithFormat:@"%@",thisProduct.termsAndConditions];
    
    NSString *myDescriptionHTML=[NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "<body>%@</body> \n"
                                 "</html>", @"Helvetica", [NSNumber numberWithInt:13],webViewtext];
    
    [tcWebView loadHTMLString:myDescriptionHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    tcWebView.tag = 2013;
    //offerWebView.delegate = self;
    [tcView addSubview:tcWebView];
    
}


- (void) addOfferViewAsSubview {
    
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = offerView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:self.offerView];
    [self updateScrollerContentSize];
}
- (void) addAddressViewAsSubview {
    
    addressView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:addressView];
    [self updateScrollerContentSize];
    
}
-(void)addTCViewAsSubview{
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = tcView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:self.tcView];
    [self updateScrollerContentSize];
}

- (void) updateUIWithProductDetails:(Product *) aProduct {
    
    [self updateOfferView];
}

- (void) updateAddressView {
    
    [self fetchMerchantWithProductID:thisProduct.productId];
    
}
- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    if(addressDetailsLoaded) {
        // Already address is processed.
        return;
    }
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,productId];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    merchantFetchRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}


//=================

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    [_responseData appendData:data];
    
    NSString *str = [[NSString alloc]initWithData:_responseData encoding:NSUTF8StringEncoding];
    NSLog(@"RESPONSE::%@",str);
    if(connection == conn)
    {
        
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:_responseData];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
    
    else if(connection == proddImage) 
    {
        
        UIImage *image = [UIImage imageWithData:_responseData];
        [self.imageView setImage:image];
        
        
    }
    else if(connection == merchantFetchRequest) {
        
        NSLog(@"==merchantFetchRequest==");
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:data];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        addressDetailsLoaded = YES;
    }

        
    
}


- (void)parsingProductDataFinished:(Product *)product {
    
    NSLog(@"parsingProductDataFinished");
    
    
    thisProduct = product;
    [self updateFavoriteStatus];
    
    
    [self updateUIWithProductDetails:thisProduct];
   // [self offerButtonPressed:offerButton];  code for segment control first button clicked..
    //self.segmentedControl.selectedSegmentIndex = 0;
    [self segmentedControlIndexChanged:0];
    
    NSLog(@"productName::%@",thisProduct.productName);
    NSLog(@"productOffer::%@",thisProduct.productOffer);
    
    self.productNameLabel.text = thisProduct.productName;
    self.productOfferLabel.text = thisProduct.productOffer;
    self.startDate_Label.text = [NSString stringWithFormat:@"Starts \n %@",thisProduct.product_StartDate];
    self.endDate_Label.text = [NSString stringWithFormat:@"Between \n %@",thisProduct.product_EndDate];
    self.productsLeftCount_Label.text = [NSString stringWithFormat:@"%d \n Left",thisProduct.product_AvailableCount];
    self.hoursCountDown_Label.text = [NSString stringWithFormat:@"Hours countDown \n %d",thisProduct.product_HoursCountDown];
    
    [self dismissActivityView];
    
    if (thisProduct.productImage)
    {
        
        [self.imageView setImage:thisProduct.productImage];
        
    }
    else
    {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:thisProduct.productImgLink]];
        proddImage = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
    }
    
}
- (void) parsingProductDataXMLFailed{
    NSLog(@"XML failed");
}
//==================================

-(BOOL)doesString:(NSString *)string containCharacter:(char)character
{
    if ([string rangeOfString:[NSString stringWithFormat:@"%c",character]].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    NSLog(@"\n Merchants lIst in  parsingDataFinished metnod = %d",[merchantsListLocal count]);
    
    if (!merchantList) {
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    }
    else {
        [merchantList addObjectsFromArray:merchantsListLocal];
    }
    
    [merchantTableView reloadData];
}

- (void)parsingMerchantListXMLFailed {
    
}
#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n MerchantsList count = %d",[merchantList count]);
    return [merchantList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == merchantList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    MerchantAddressCell *cell = (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    
    
    cell= (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
    if(cell==nil)
    {
        
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    
        
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    NSLog(@"Address: %@",aMerchant.mMailAddress1);
    NSLog(@"Suburb:%@",aMerchant.mMailSuburb);
    NSLog(@"State:%@",aMerchant.mState);
    NSLog(@"Phone:%@",aMerchant.mPhone);
    NSLog(@"=============================>>>aMerchant.mId:%@",aMerchant.mId);
    
    
    if([aMerchant.mMailAddress1 length]<=0 && [merchantList count] == 1){
        NSLog(@"null");
        cell.merchantAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str = aMerchant.mMailAddress1;
        
        if([aMerchant.mMailSuburb length]>0)
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        cell.merchantAddress.text = str;
        cell.mapButton.hidden = NO;
        cell.mapLabel.hidden = NO;
    }
    [cell.mapButton addTarget:self action:@selector(mapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.merchantAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    
    
    NSLog(@"%f,%f",Location.latitude,Location.longitude);
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}
-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}




//==============================================================================================================//


//===========================================

-(void)facebookButton_Action
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *str = [NSString stringWithFormat:@"%@ \n %@",thisProduct.productName,thisProduct.productOffer];
        [controller setInitialText:str];
        [controller addImage:self.imageView.image];
        [self presentViewController:controller animated:YES completion:Nil];
    }
        
}
    
    
-(void)twitterButton_Action{

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString *str = [NSString stringWithFormat:@"%@ \n %@",thisProduct.productName,thisProduct.productOffer];
        [tweetSheet setInitialText:str];
        [tweetSheet addImage:self.imageView.image];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}


-(void)emailButton_Action{
    
    if ([MFMailComposeViewController canSendMail])
    {
    MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
    composer.mailComposeDelegate = self;
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(self.imageView.image)];
    [composer addAttachmentData:imageData mimeType:@"image/png" fileName:@"photo"];
    NSString *emailBody = [NSString stringWithFormat:@"%@ \n %@",thisProduct.productName,thisProduct.productOffer];
    [composer setMessageBody:emailBody isHTML:NO];

    NSArray *arr=[[NSArray alloc]initWithObjects:@"prashanthanits@yahoo.com", nil];
    [composer setToRecipients:arr];
    
    [self presentViewController:composer animated:YES completion:nil];
    }
    else
    {
        // Show some error message here
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        return;
    }


}
-(void)phoneButton_Action{

          phArray = [[NSMutableArray alloc]init];
          NSLog(@"phno::%@",thisProduct.phone);
          

    
       if([thisProduct.phone length]>15)
        {
        phnoCount = 2;
        NSLog(@"ph numbers Count:%d",phnoCount);
        if ([self doesString:thisProduct.phone containCharacter:'/'])// phNo having character '/'
        {
            
            NSArray *phNo = [thisProduct.phone componentsSeparatedByString:@"/"];
             //NSLog(@"phone No1::%@",[phNo objectAtIndex:0]);
             //NSLog(@"phone No2::%@",[phNo objectAtIndex:1]);
            
            NSString *ph1 = [phNo objectAtIndex:0];
            NSString *ph2 = [[ph1 substringToIndex:[ph1 length] - 3] substringFromIndex:0];
            // NSLog(@"ph2::%@",ph2);
            
            NSString *newString = [[phNo objectAtIndex:1]stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSMutableString *ph2formated = [[NSMutableString alloc]initWithString:ph2];
            [ph2formated appendString:newString];
            
            [phArray addObject:[phNo objectAtIndex:0]];
            [phArray addObject:ph2formated];
            
            
            // NSLog(@"ph2::%@",ph2formated);
        }
        
        else  // phNo having character ','
        {
            
            NSArray *myArray = [thisProduct.phone componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/, "]];
            //NSLog(@"phone No1::%@",[myArray objectAtIndex:0]);
             //NSLog(@"phone No2::%@",[myArray objectAtIndex:2]);
            
            NSArray *listItems = [thisProduct.phone componentsSeparatedByString:@"-"];
            NSString *code = [listItems objectAtIndex:0];
            // NSLog(@"code is ::%@",code);
            NSString *formatedPhNo = [NSString stringWithFormat:@"%@-%@",code,[myArray objectAtIndex:2]];
            // NSLog(@"phone No2::%@",formatedPhNo);
            
            [phArray addObject:[myArray objectAtIndex:0]];
            [phArray addObject:formatedPhNo];
        }
    }
    else
    {
        if([thisProduct.phone length] <= 0)
        {
            phnoCount = 0;
           // NSLog(@"ph numbers Count:%d",phnoCount);
        }
        else
        {   phnoCount = 1;
           // NSLog(@"ph numbers Count:%d",phnoCount);
            [phArray addObject:thisProduct.phone];
        }
    }


    NSString *callNumber =[phArray objectAtIndex:0];
    
        
    MyCustomAlert *call_Alert = [[MyCustomAlert alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call" , nil];
    call_Alert.tag = 111;
    [call_Alert show];






}
-(void)bookNowButton_Action{}

-(void)websiteButton_Action
     {
              NSLog(@"website link::%@",thisProduct.websiteLink);
              NSURL *websiteUrl = [NSURL URLWithString:thisProduct.websiteLink];
    
              [[UIApplication sharedApplication] openURL:websiteUrl];
}

-(void)commentButton_Action{}
-(void)scorePointsButton_Action{}
-(void)redeemQRButton_Action{
    
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    //  [self presentModalViewController: reader
    // animated: YES];
    [self presentViewController:reader animated:YES completion:NULL];

}
-(void)redeemPasswordButton_Action{
    
    
    if (IOS_VERSION >= 7)
    {
        NSLog(@"firsttimeLogin_ButtonTapped ios 7");
        [self launchDialog];
    }
    else
    {
  MyCustomAlert  *myAlert = [[MyCustomAlert alloc] initWithTitle:@"Enter Redeem pin number below." message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
    
    myAlert.tag = 222;
    self.redeemPin_TextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 65.0, 240.0, 25.0)];
    self.redeemPin_TextField .placeholder = @"Pin Number";
    self.redeemPin_TextField .font = [UIFont systemFontOfSize:14];
    self.redeemPin_TextField .borderStyle = UITextBorderStyleRoundedRect;
    [self.redeemPin_TextField  setBackgroundColor:[UIColor whiteColor]];
    
  //  NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
        //[self.redeemPin_TextField textFieldAtIndex:0];
    [myAlert addSubview:self.redeemPin_TextField ];
    [myAlert show];
    }

}
- (void)launchDialog
{
    // Here we need to pass a full frame
    alertView7 = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView7 setContainerView:[self createDemoView]];
    
    // Modify the parameters
    [alertView7 setButtonTitles:[NSMutableArray arrayWithObjects:@"Close", @"Submit", nil]];
   [alertView7 setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    /*   [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
     NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
     [alertView close];
     }];  */
    
    [alertView7 setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView7 show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView7 tag]);
    NSLog(@" ");
    if(buttonIndex == 0)
    {
        [alertView7 close];
        if (alertView7.tag == 222)
        {
//            usernameTextField.text = userName;
//            subDomainTextField.text = subDomainName;
          //  [self closeButton_Tapped:nil];
        }
        
    }
    else
    {
        // [alertView close];
        if ([self.redeemPin_TextField.text length] == 0 )
        {
            alertView7.direction = 1;
            alertView7.shakes = 0;
            [alertView7 shake];
        }
        else
        {
            NSLog(@"enter pin ");
        }
        
    }
}



- (UIView *)createDemoView
{
    demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 275, 170)];
    NSLog(@"creratedemoview.... ");
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    
    UILabel *alertlabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 240, 70)];
    alertlabel.numberOfLines = 3;
    //alertlabel.font = [UIFont fontNamesForFamilyName:@"System Bold"];
    alertlabel.font = [UIFont boldSystemFontOfSize:17];
    alertlabel.textAlignment = NSTextAlignmentCenter;
    alertlabel.text = @"Enter Redeem pin number below.";
    [demoView addSubview:alertlabel];
    
    redeemPin_TextField = [[UITextField alloc]initWithFrame:CGRectMake(25, 120, 230, 30)];
   // redeemPin_TextField.delegate = self;
    redeemPin_TextField.borderStyle = UITextBorderStyleRoundedRect;
    redeemPin_TextField.placeholder = @"Pin Number";
    [demoView addSubview:redeemPin_TextField];
    
    return demoView;
}

//=====================

//============================-----------scanner method ------------============================//
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    // mytextView .text = symbol.data;
//    mytextView .text = symbol.data;
      NSLog(@"symbol.data::%@",symbol.data);
//    
//    // EXAMPLE: do something useful with the barcode image
//    myImageView.image =
//    [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    //[reader dismissModalViewControllerAnimated: YES];
    [reader dismissViewControllerAnimated:YES completion:NULL];
    
    
    if([symbol.typeName isEqualToString:@"QR-Code"]){
        
        NSURL *websiteUrl = [NSURL URLWithString:symbol.data];
        
        [[UIApplication sharedApplication] openURL:websiteUrl];}
    
    else if([symbol.typeName isEqualToString:@"EAN-13"]){
        
    }
}
//===============------------------- SHAKE EFFECT TO UIALERTVIEW ----------------===================//
-(void)shake:(MyCustomAlert *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

- (void)alertView:(MyCustomAlert *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
    
    if (alertView.cancelButtonIndex == buttonIndex) { alertView.dontDisppear = NO; }
    else
    {
        if(alertView.tag == 111)
        {
                NSLog(@"call alert view and button index::%d",buttonIndex);
            
                NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@",thisProduct.phone];
                NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
                [[UIApplication sharedApplication] openURL:phoneURL];
            
        }
        
        else if(alertView.tag == 21){
            // Add to fav case
             [appDelegate addProductToFavorites:thisProduct];
            
            [self updateFavoriteStatus];
            
            
           // if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
               // [delegate productAddedToFavorites:thisProduct];
            //}
        }
        else if(alertView.tag == 22){
            // Remove from fav case
             [appDelegate removeProductFromfavorites:thisProduct];
            
            [self updateFavoriteStatus];
            
           // if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
              //  [delegate productremovedFromFavorites:thisProduct];
           // }

        }
        else{
            alertView.dontDisppear = YES;
            if ([self.redeemPin_TextField.text length] == 0 )
            {
                
                direction = 1;
                shakes = 0;
                [self shake:alertView];
                
                [alertView setTitle:@" \n \n"];
                UILabel *alertLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(15, 20, 250.0, 40.0)];
                alertLabel.textAlignment =  UITextAlignmentCenter;
                alertLabel.textColor = [UIColor redColor];
                alertLabel.backgroundColor = [UIColor clearColor];
                alertLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(18.0)];
                alertLabel.numberOfLines = 2;
                alertLabel.text = @"Pin Number required";
                [alertView addSubview:alertLabel];
            }
            
        }

      }
}



- (void) updateFavoriteStatus {
      NSLog(@"updateFavoriteStatus");
    if ([appDelegate productExistsInFavorites:thisProduct]){
        NSLog(@"heart_full available");
        [favButton setImage:[UIImage imageNamed:@"heart_full.png"] forState:UIControlStateNormal];
    }
    else {
        NSLog(@"heart_empty Not available");
        [favButton setImage:[UIImage imageNamed:@"heart_empty.png"] forState:UIControlStateNormal];
    }
}


#pragma WebViewDelegate methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // [loadingView setHidden:YES];
    if (webView.tag == 2012) {
        
        UIWebView *offerWebView = webView;
        
        CGRect modifiedFrame = offerWebView.frame;
        modifiedFrame.size.height = offerWebView.scrollView.contentSize.height;
        offerWebView.frame = modifiedFrame;
        
        //  webView.scalesPageToFit=YES;
        //  webView.multipleTouchEnabled=YES;
        
        NSLog(@"webview frame = %@, webview content size = %@",NSStringFromCGRect(offerWebView.frame),NSStringFromCGSize(offerWebView.scrollView.contentSize));
        
        CGRect modifiedOfferViewFrame = offerView.frame;
        modifiedOfferViewFrame.size = offerWebView.frame.size;
        offerView.frame = modifiedOfferViewFrame;
        
        if ([offerView superview]) {
            [self addOfferViewAsSubview];
        }
        
    }
    
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

    
//============================ SHOW ALERTVIEW METHODS  =============================//
- (void) showActivityView {
    //activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}
- (void) dismissActivityView {
    [activityView removeFromSuperview];
}
//==================================================================================//
    
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
