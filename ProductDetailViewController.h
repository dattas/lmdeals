//
//  ProductDetailViewController.h
//  MyARSample
//
//  Created by vairat on 04/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Product.h"
#import "ProductDataParser.h"
#import "MerchantListParser.h"
#import "Merchant.h"
#import "NSString_stripHtml.h"
#import "ZBarSDK.h"
#import "CustomIOS7AlertView.h"

@interface ProductDetailViewController : UIViewController <ZBarReaderDelegate, ProductXMLParserDelegate,MerchantXMLParserDelegate, UIWebViewDelegate, UIAlertViewDelegate,  UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate,CustomIOS7AlertViewDelegate>{
    
    NSMutableData *_responseData;
    NSMutableArray *merchantList;
    CLLocationCoordinate2D Location;
    
    int phnoCount;
    NSMutableArray *phArray;
}
@property(nonatomic,readwrite)CLLocationCoordinate2D Location;
@property (nonatomic,strong) IBOutlet UILabel * productNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *productOfferLabel;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic,strong) IBOutlet UIView * mainContentHolder;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic,strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) UITableView *merchantTableView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic,strong) IBOutlet UIScrollView *scroller;
@property (nonatomic,strong) IBOutlet UIView *imgContainerView;
@property (nonatomic,strong) IBOutlet UIView *sideSlideView;
@property (nonatomic,strong) IBOutlet UIButton *favButton;
@property (nonatomic,strong) UITextField *redeemPin_TextField;

@property(nonatomic, retain)IBOutlet UILabel *endDate_Label;
@property(nonatomic, retain)IBOutlet UILabel *startDate_Label;
@property(nonatomic, retain)IBOutlet UILabel *productsLeftCount_Label;
@property(nonatomic, retain)IBOutlet UILabel *hoursCountDown_Label;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productID:(NSString *)ProId;
-(IBAction)buttonTapped_Actions:(id)sender;
-(IBAction)sideMenuTapped_Actions:(id)sender;
-(IBAction) segmentedControlIndexChanged: (id)sender;
@end
