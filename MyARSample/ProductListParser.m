//
//  ProductListParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductListParser.h"
#import "Product.h"
#import <CoreLocation/CoreLocation.h>

@interface ProductListParser()
{
    NSMutableArray *productsList;
    NSMutableString *charString;
    Product *currProduct;
}

@property (nonatomic, strong) NSMutableArray *productsList;

@end


@implementation ProductListParser
@synthesize delegate;
@synthesize productsList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    productsList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"product"]||[elementName isEqualToString:@"deals"]) {
        currProduct = [[Product alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        currProduct.productId = finalString;
        
        // construct image link
        NSString *imgLink = [NSString stringWithFormat:@"%@%@.jpg",Image_URL_Prefix,finalString];
        currProduct.productImgLink = imgLink;
        
    }
    else if ([elementName isEqualToString:@"name"]) {
        currProduct.productName = finalString;
    }
    else if ([elementName isEqualToString:@"highlight"]) {
        currProduct.productOffer = finalString;
    }
    else if ([elementName isEqualToString:@"latitude"]) {
        
        CLLocationCoordinate2D localCoo = currProduct.coordinate;
        localCoo.latitude = [finalString doubleValue];
        currProduct.coordinate = localCoo;
    }
    else if ([elementName isEqualToString:@"longitude"]) {
        
        CLLocationCoordinate2D localCoo = currProduct.coordinate;
        localCoo.longitude = [finalString doubleValue];
        currProduct.coordinate = localCoo;
    }
    else if ([elementName isEqualToString:@"pin_type"]) {
        currProduct.pin_type = finalString;
    }
    else if ([elementName isEqualToString:@"text"]) {
        
        currProduct.productText = finalString;
    }
    //==============================================
    else if ([elementName isEqualToString:@"highlights"]) {
        currProduct.productOffer = finalString;
    }
    else if ([elementName isEqualToString:@"count"]) {
        currProduct.product_HoursCountDown = [finalString intValue];
    }
    else if ([elementName isEqualToString:@"start_date"]) {
        currProduct.product_StartDate = finalString;
    }
    else if ([elementName isEqualToString:@"end_date"]) {
        currProduct.product_EndDate = finalString;
    }
    else if ([elementName isEqualToString:@"active"]) {
        currProduct.product_Active = [finalString intValue];
    }
    
    //=============================================
    else if ([elementName isEqualToString:@"product"]||[elementName isEqualToString:@"deals"]) {
        NSLog(@"currProduct isd %@ ",currProduct);
        if (currProduct)
        {
            [productsList addObject:currProduct];
        }
       
        currProduct = nil;
        
    }
    else if([elementName isEqualToString:@"link_next"])
    {
        NSLog(@"link next ignore  ");
    }
    else if([elementName isEqualToString:@"link_prev"])
    {
         NSLog(@"link prev ignore  ");
    }
    else if([elementName isEqualToString:@"root"]) {
        NSLog(@"root as element ");
        [parser abortParsing];
        [delegate parsingProductListFinished:productsList];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [delegate parsingProductListXMLFailed];
}

@end

/*
 <root>
    <product>
        <id>1016469</id>
        <name>American Tourister Luggage</name>
        <highlight>Save up to $100!</highlight>
    </product>
    <link_next>search.php?cid=24&amp;q=&amp;start=10&amp;limit=10</link_next>
    <link_prev></link_prev>
 </root>
 
 */

