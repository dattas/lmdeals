//
//  PostADealViewController.m
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PostADealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PostADealNextViewController.h"
#import "MerchantAddressCell.h"
#import "Category.h"
#import "AppDelegate.h"
#define kOFFSET_FOR_KEYBOARD 215

@interface PostADealViewController (){
    
    AppDelegate *appDelegate;
    
    UILabel *productNamePlaceHolder;
    UILabel *productOfferPlaceHolder;
    UILabel *productOfferDetailPlaceHolder;
    UILabel *productTCPlaceHolder;
    
    UIView *offerView;
    UITextView *offerTextView;
    
    UIView *tcView;
    UITextView *tcTextView;
    
    UIDatePicker *myDatepicker;
    UIPickerView  *searchCriteriaPicker;
    UIActionSheet *myActionSheet;
    NSMutableArray *productLeftItems_Array;
    NSMutableArray *hoursCountDown_Array;
    NSMutableArray *searchCriteriaLabels_Array;
    NSArray *searchCriteriaItems;
    
    //NSArray *segmentedControlImages_Array;
   // NSArray *segmentedControlHihglightImages_Array;
    NSString *gridLabelValue;
    NSString *routePath;
    
    int currentDateButton;  // 1 for StartTime, 2 for EndTime, 3 for Category
    BOOL isViewUp;
    
    MerchantListParser *merchantListParser;
    PostADealNextViewController *postaDealNext;
    NSURLConnection *merchantFetchRequest;
   
    
    NSString *productLeftCountStr;
    NSString *hoursCountDownStr;
    NSString *productCatId;
    NSString *startDateStr;
    NSString *endDateStr;
    int merchantAddressId;
    BOOL isValidPost;
    UIView *pickerContainerView;
    UIView *datePickerContainerView;

    
    
}
@property(nonatomic,retain)NSURLConnection *merchantFetchRequest;
@property(nonatomic, retain)NSMutableArray *productLeftItems_Array;
@property(nonatomic, retain)NSMutableArray *hoursCountDown_Array;
@property(nonatomic, retain)NSMutableArray *searchCriteriaLabels_Array;
@property(nonatomic,retain)NSArray *searchCriteriaItems;
@property (nonatomic, strong) UIView *offerView;
@property (nonatomic, strong) UIView *tcView;
@end

@implementation PostADealViewController
@synthesize productName_TextView;
@synthesize productOffer_TextView;
@synthesize current_TextView;
@synthesize accessoryView;
@synthesize endDate_Label;
@synthesize product_ImageView;
@synthesize productsLeft_PickerView;
@synthesize hoursCountDown_PickerView;
@synthesize productLeftItems_Array;
@synthesize hoursCountDown_Array;
@synthesize searchCriteriaItems;
@synthesize startDate_Label;
@synthesize segmentedControl;
@synthesize mainContentHolder;
@synthesize offerView;
@synthesize tcView;
@synthesize merchantTableView;
@synthesize addressAdd_View;
@synthesize productLeftCount_Label;
@synthesize productCategory_Label;
@synthesize searchCriteriaLabels_Array;
@synthesize Location;
@synthesize hoursCountDown_Label;
@synthesize current_PostingProduct;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil throughView:(NSString *)through
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        routePath = through;
        productLeftItems_Array = [[NSMutableArray alloc] init];
        hoursCountDown_Array = [[NSMutableArray alloc] init];
        searchCriteriaLabels_Array = [[NSMutableArray alloc] init];
        currentDateButton = -1;
        isViewUp = NO;
        isValidPost = NO;
        gridLabelValue = @"";
        
//        segmentedControlImages_Array = [[NSArray alloc]initWithObjects:@"Offer.png",@"T&C.png",@"Stores.png", nil];
//        segmentedControlHihglightImages_Array = [[NSArray alloc]initWithObjects:@"Offer_Highlight.png",@"T&C_Highlight.png",@"Stores_Highlight.png", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    productLeftCountStr = @"1";
    hoursCountDownStr = @"1";
    productCatId = @"10";
     merchantAddressId = 0;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
   
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Next"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(nextButtonTapped:)];
    
    self.searchCriteriaItems = [[NSArray alloc]initWithObjects:@"Automotive",@"Dining & FastFood",@"Golf Courses",@"Healthy & Beauty",@"Home & lifestyle",@"Leisure & Entertainment",@"Shopping & Vochers",@"Travel & Accommodation", nil];
    
    productNamePlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(14, 7, 200, 30)];
    productNamePlaceHolder.text = @"Product Name";
    productNamePlaceHolder.backgroundColor = [UIColor clearColor];
    productNamePlaceHolder.font = [UIFont systemFontOfSize:14];
    productNamePlaceHolder.textColor = [UIColor lightGrayColor];
    [self.productName_TextView addSubview:productNamePlaceHolder];
    
    productOfferPlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(14, 7, 200, 30)];
    productOfferPlaceHolder.text = @"Product Offer";
    productOfferPlaceHolder.backgroundColor = [UIColor clearColor];
    productOfferPlaceHolder.font = [UIFont systemFontOfSize:14];
    productOfferPlaceHolder.textColor = [UIColor lightGrayColor];
    [self.productOffer_TextView addSubview:productOfferPlaceHolder];
    
    
    product_ImageView.layer.borderWidth = 2.0;
    product_ImageView.layer.borderColor = [UIColor blackColor].CGColor;
    product_ImageView.layer.cornerRadius = 1.0;
    
    endDate_Label.layer.borderWidth = 2.0;
    endDate_Label.layer.borderColor = [UIColor blackColor].CGColor;
    endDate_Label.layer.cornerRadius = 4.0;
    
    startDate_Label.layer.borderWidth = 2.0;
    startDate_Label.layer.borderColor = [UIColor blackColor].CGColor;
    startDate_Label.layer.cornerRadius = 4.0;
    
    hoursCountDown_Label.layer.borderWidth = 2.0;
    hoursCountDown_Label.layer.borderColor = [UIColor blackColor].CGColor;
    hoursCountDown_Label.layer.cornerRadius = 4.0;
    
    productLeftCount_Label.layer.borderWidth = 2.0;
    productLeftCount_Label.layer.borderColor = [UIColor blackColor].CGColor;
    productLeftCount_Label.layer.cornerRadius = 4.0;
    
    productCategory_Label.layer.borderWidth = 2.0;
    productCategory_Label.layer.borderColor = [UIColor blackColor].CGColor;
    productCategory_Label.layer.cornerRadius = 4.0;
    
    productName_TextView.layer.borderWidth = 2.0;
    productName_TextView.layer.borderColor = [UIColor blackColor].CGColor;
    productName_TextView.layer.cornerRadius = 4.0;
    
    productOffer_TextView.layer.borderWidth = 2.0;
    productOffer_TextView.layer.borderColor = [UIColor blackColor].CGColor;
    productOffer_TextView.layer.cornerRadius = 4.0;
    
//======================== HoRIZONTAL PICKER
    
    
    self.productsLeft_PickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 0.0, 290.0, 120.0)];
    self.productsLeft_PickerView.dataSource = self;
    self.productsLeft_PickerView.delegate = self;
	self.productsLeft_PickerView.showsSelectionIndicator =YES;
	self.productsLeft_PickerView.backgroundColor = [UIColor clearColor];
    
    
    self.hoursCountDown_PickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 0.0, 290.0, 120.0)];
    self.hoursCountDown_PickerView.dataSource = self;
    self.hoursCountDown_PickerView.delegate = self;
	self.hoursCountDown_PickerView.showsSelectionIndicator =YES;
	self.hoursCountDown_PickerView.backgroundColor = [UIColor clearColor];
    

	CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
	rotate = CGAffineTransformScale(rotate, 0.1, 0.8);
	[self.productsLeft_PickerView setTransform:rotate];
    [self.hoursCountDown_PickerView setTransform:rotate];
	
	self.productsLeft_PickerView.center = CGPointMake(238,170);
    self.hoursCountDown_PickerView.center = CGPointMake(85,170);
    
	
	UILabel *theview;
	CGAffineTransform rotateItem = CGAffineTransformMakeRotation(-3.14/2);
	rotateItem = CGAffineTransformScale(rotateItem, 1, 10);
	
	for (int i=1;i<=100;i++) {
		theview = [[UILabel alloc] init];
		theview.text = [NSString stringWithFormat:@"%d",i];
		theview.textColor = [UIColor blackColor];
		theview.frame = CGRectMake(0,0,100,100);
		theview.backgroundColor = [UIColor clearColor];
		theview.textAlignment = UITextAlignmentCenter;
		theview.shadowColor = [UIColor clearColor];
		theview.shadowOffset = CGSizeMake(-1,-1);
		theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:15];
		[theview setFont:myFont];
		
		theview.transform = rotateItem;
        [productLeftItems_Array addObject:theview];
	}
    for (int i=1;i<=24;i++) {
		theview = [[UILabel alloc] init];
		theview.text = [NSString stringWithFormat:@"%d",i];
		theview.textColor = [UIColor blackColor];
		theview.frame = CGRectMake(0,0,100,100);
		theview.backgroundColor = [UIColor clearColor];
		theview.textAlignment = UITextAlignmentCenter;
		theview.shadowColor = [UIColor whiteColor];
		theview.shadowOffset = CGSizeMake(-1,-1);
		theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:15];
		[theview setFont:myFont];
		
		theview.transform = rotateItem;
        [hoursCountDown_Array addObject:theview];
	}

    
    
    
    for (int i=0;i<=7;i++) {
        theview = [[UILabel alloc] init];
        
        Category *catObj = [appDelegate.categoryList_Array objectAtIndex:i];
		theview.text = [NSString stringWithFormat:@"%@",catObj.catName];
		theview.textColor = [UIColor blackColor];
		theview.frame = CGRectMake(0,0,200,200);
		theview.backgroundColor = [UIColor clearColor];
		theview.textAlignment = UITextAlignmentCenter;
		//theview.shadowColor = [UIColor whiteColor];
		//theview.shadowOffset = CGSizeMake(-1,-1);
		theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:15];
		[theview setFont:myFont];

        [self.searchCriteriaLabels_Array addObject:theview];
    
    }
	
    CALayer* mask = [[CALayer alloc] init];
    mask.backgroundColor = [UIColor blackColor].CGColor;
    mask.frame = CGRectInset(self.productsLeft_PickerView.bounds, 10.0f, 10.0f);
    mask.cornerRadius = 5.0f;
    self.productsLeft_PickerView.layer.mask = mask;
    
    CALayer* mask1 = [[CALayer alloc] init];
    mask1.backgroundColor = [UIColor blackColor].CGColor;
    mask1.frame = CGRectInset(self.hoursCountDown_PickerView.bounds, 10.0f, 10.0f);
    mask1.cornerRadius = 5.0f;
    self.hoursCountDown_PickerView.layer.mask = mask1;
    [self.productsLeft_PickerView setShowsSelectionIndicator:YES];
    [self.productsLeft_PickerView setShowsSelectionIndicator:YES];

    [self.view addSubview:self.productsLeft_PickerView];
	[self.view addSubview:self.hoursCountDown_PickerView];
    mainContentHolder.layer.cornerRadius = 4.0;
    self.addressAdd_View.layer.cornerRadius = 4.0;
    [self segmentedControlIndexChanged:0];
    
    if([routePath isEqualToString:@"Menu"])
        self.title = @"Post A Deal";
    
    else{
        self.title = @"Edit A Deal";
        
        
        productNamePlaceHolder.hidden = YES;
        productOfferPlaceHolder.hidden = YES;
        
        self.productName_TextView.text = current_PostingProduct.productName;
        self.productOffer_TextView.text = current_PostingProduct.productOffer;
        offerTextView.text = current_PostingProduct.productDesciption;
        tcTextView.text = current_PostingProduct.termsAndConditions;
        
        
     [productsLeft_PickerView selectRow:[self getIndexOfProAvailCount:current_PostingProduct.product_AvailableCount] inComponent:0 animated:NO];
        
    [hoursCountDown_PickerView selectRow:[self getIndexOfProHoursCountDown:current_PostingProduct.product_HoursCountDown] inComponent:0 animated:NO];
        
        //NSLog(@"current_PostingProduct.product_CategoryId::%@",current_PostingProduct.product_CategoryId);
        self.productCategory_Label.text = [self getCategoryWithID:current_PostingProduct.product_CategoryId];
        
        self.startDate_Label.text = current_PostingProduct.product_StartDate;
        self.endDate_Label.text = current_PostingProduct.product_EndDate;
        
    
        
    }
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTranslucent:NO];
}
-(int)getIndexOfProAvailCount:(int)value{
    
    for (int i =0; i < [productLeftItems_Array count]; i++) {
        
        UILabel *myLabel = [productLeftItems_Array objectAtIndex:i];
        
        if ([myLabel.text isEqualToString:[NSString stringWithFormat:@"%d",value]]) 
            return i;
        
    }
    return -1;
}
-(int)getIndexOfProHoursCountDown:(int)value{
    
    for (int i =0; i < [hoursCountDown_Array count]; i++) {
        
        UILabel *myLabel = [hoursCountDown_Array objectAtIndex:i];
        
        if ([myLabel.text isEqualToString:[NSString stringWithFormat:@"%d",value]])
            return i;
        
    }
    return -1;
}


-(NSString *)getCategoryWithID:(NSString *)cat_ID{
    
    
    for (int i =0; i < [appDelegate.categoryList_Array count]; i++) {
        
        Category *catObj = [appDelegate.categoryList_Array objectAtIndex:i];
        
        if ([catObj.catId isEqualToString:cat_ID])
            return catObj.catName;
        
    }
    
    return @"";
    
}

//====================================BUTTON ACTIONS =================================//

-(void)nextButtonTapped:(id)sender {
    
    
    if([routePath isEqualToString:@"Menu"]){
        
      current_PostingProduct = [[Product alloc]init];
    }
    else{
        startDateStr = self.startDate_Label.text;
        endDateStr   = self.endDate_Label.text;
    }
    current_PostingProduct.productName               = [productName_TextView.text     stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    current_PostingProduct.productOffer              = [productOffer_TextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    current_PostingProduct.product_StartDate         = startDateStr;
    current_PostingProduct.product_EndDate           = endDateStr;
    current_PostingProduct.product_AvailableCount    = [productLeftCountStr intValue];
    current_PostingProduct.product_HoursCountDown    = [hoursCountDownStr intValue];
    current_PostingProduct.product_CategoryId        = productCatId;
    current_PostingProduct.productDetailedDesciption = [offerTextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    current_PostingProduct.termsAndConditions        = [tcTextView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    current_PostingProduct.StoreId                   = merchantAddressId;
    
    current_PostingProduct.productImage  = self.product_ImageView.image;
    NSLog(@"current_PostingProduct.productName:::%@",current_PostingProduct.productName);
    NSLog(@"current_PostingProduct.product_CategoryId:::%@",current_PostingProduct.product_CategoryId);
    
   
    postaDealNext = [[PostADealNextViewController alloc]initWithNibName:@"PostADealNextViewController" bundle:Nil postProduct:current_PostingProduct throughView:routePath];
    [self.navigationController pushViewController:postaDealNext animated:YES];
    
}

-(IBAction)gridButton_Pressed:(id)sender{
    
//    myActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
//    myActionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    if ([sender tag] != 3)   //for start and end time buttons
    {
        myDatepicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
       //[myDatepicker setMinimumDate: [NSDate date]];
       myDatepicker.datePickerMode = UIDatePickerModeDate;
      // [myActionSheet addSubview:myDatepicker];
       currentDateButton = [sender tag]; //to identify the corresponding Label to assign date
       [myDatepicker setFrame:CGRectMake(0, 50, 320, 250)];
        //[myDatepicker setFrame:CGRectMake(0, 50, 320, 250)];
        // [searchCriteriaActionSheet addSubview:searchCriteriaPicker];
        myDatepicker.backgroundColor=[UIColor yellowColor];
        
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
        [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag=330;
        
        myDatepicker.backgroundColor=[UIColor whiteColor];
        datePickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
        datePickerContainerView.backgroundColor = [UIColor whiteColor];
        [datePickerContainerView addSubview:searchCriteriaPicker];
        [datePickerContainerView addSubview:button];
        [self.view addSubview:datePickerContainerView];
    }
    else{
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
        [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
         button.tag=440;
        // for ProductCategory button
        searchCriteriaPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
        searchCriteriaPicker.delegate = self;
        searchCriteriaPicker.dataSource = self;
        searchCriteriaPicker.showsSelectionIndicator = YES;
        searchCriteriaPicker.backgroundColor=[UIColor clearColor];
        [searchCriteriaPicker setFrame:CGRectMake(0, 50, 320, 250)];
        searchCriteriaPicker.backgroundColor=[UIColor clearColor];
        pickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
        pickerContainerView.backgroundColor = [UIColor whiteColor];
        [pickerContainerView addSubview:searchCriteriaPicker];
        [pickerContainerView addSubview:button];
        [self.view addSubview:pickerContainerView];
    }
             
   
//    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
//    tools.barStyle=UIBarStyleBlackOpaque;
//    [myActionSheet addSubview:tools];
//    
//    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
//    doneButton.tag = [sender tag];
//    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
//    
//    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    
//    NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
//    [tools setItems:array];
//       
//
//    //picker title
//    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
//    if([sender tag] == 2)
//        lblPickerTitle.text=@"Select Category";
//    else
//        lblPickerTitle.text=@"Select Date";
//    lblPickerTitle.backgroundColor=[UIColor clearColor];
//    lblPickerTitle.textColor=[UIColor whiteColor];
//    lblPickerTitle.textAlignment=UITextAlignmentCenter;
//    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
//    [tools addSubview:lblPickerTitle];
//    
//    
//    [myActionSheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
//    [myActionSheet setBounds:CGRectMake(0,0, 320, 500)];
    NSLog(@"gridButton_Pressed");

}
-(void)doneButtonPressed:(id)sender
{ NSLog(@"gridButton_Pressed is %d",[sender tag]);
    if ([sender tag] == 330)
        [datePickerContainerView removeFromSuperview];
    else
        [pickerContainerView removeFromSuperview];
}

-(void) btnActinDoneClicked:(id)sender
{
    NSLog(@"Picker Tools done Button...");
    if([sender tag] == 3 && [gridLabelValue isEqualToString:@""])
        self.productCategory_Label.text = @"Automotive";
    
    else 
    {
        NSString *dateString;
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-d"];
        dateString = [dateFormatter1 stringFromDate:myDatepicker.date];
        if([sender tag] == 1){
           self.startDate_Label.text = dateString;
           startDateStr = dateString;
        }
        else{
            self.endDate_Label.text = dateString;
            endDateStr = dateString;}
    }
    
    [myActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

//=============================== CAMERA & PHOTOGALLERY METHODS =========================//
-(IBAction)cameraButton_Pressed:(id)sender{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Unable to Capture image" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }else{
        // if capture image is not supported by the device or Stimaulator
        UIImagePickerController* imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        [self presentModalViewController:imagePickerController animated:YES];
    }



}
-(IBAction)photoGalleryButton_Pressed:(id)sender
{

    // Create image picker controller
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    
    
    // Allow editing of image ?
    imagePicker.allowsEditing = NO;
    // Show image picker
    [self presentViewController:imagePicker animated:YES completion:nil];


}
//================================ UIIMAGEPICKERCONTROLLER DELEGATES METHODS ============================//

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissModalViewControllerAnimated:YES];
    self.product_ImageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
}



//=========METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN ==========//
-(void)setViewMovedUp:(BOOL)movedUp
{
    NSLog(@"==========setViewMovedUp=========");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp){
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        isViewUp = YES;
       }
    else{
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        isViewUp = NO;
        }
    
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

//===============================SEGMENTEDCONTROL METHOD==================================//
-(IBAction) segmentedControlIndexChanged: (id)sender{
    
//    NSMutableString *tempStr;
//    for( int i = 0; i < 3; i++ ) {
//        tempStr = [segmentedControlImages_Array objectAtIndex:i];
//        [self.segmentedControl setImage:[UIImage imageNamed:tempStr] forSegmentAtIndex:i];
//    }
//    
//    //setting image to the selected segment section.
//    tempStr = [segmentedControlHihglightImages_Array objectAtIndex:self.segmentedControl.selectedSegmentIndex];
//    [self.segmentedControl setImage:[UIImage imageNamed:tempStr] forSegmentAtIndex:self.segmentedControl.selectedSegmentIndex];
    
    
    switch (self.segmentedControl.selectedSegmentIndex) {
            
        case 0:
             {
                    
                 if (!offerView) {
                     offerView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
                     offerView.backgroundColor = [UIColor whiteColor];
                     offerView.layer.cornerRadius = 4.0;
                     offerView.layer.borderColor = [UIColor blackColor].CGColor;
                     offerView.layer.borderWidth = 2.0;
                 }
    
                   [mainContentHolder addSubview:self.offerView];
                 
                   [self cleanMainController];
                   [self updateOfferView];
                   [self addOfferViewAsSubview];
            
            }
        break;
        case 1:
              {
                  
                  if (!tcView) {
                      tcView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
                      tcView.backgroundColor = [UIColor whiteColor];
                      tcView.layer.cornerRadius = 4.0;
                      tcView.layer.borderColor = [UIColor blackColor].CGColor;
                      tcView.layer.borderWidth = 2.0;
                  }
                 
                  [mainContentHolder addSubview:self.tcView];
                  
                  [self cleanMainController];
                  [self updateTcView];
                  [self addTCViewAsSubview];
                  
              }
        break;
        case 2:
             {
                 
                 [self cleanMainController];
                 
                 self.addressAdd_View.layer.borderColor = [UIColor blackColor].CGColor;
                 self.addressAdd_View.layer.borderWidth = 2.0;
                 
                  [mainContentHolder addSubview:self.addressAdd_View];
                 [mainContentHolder bringSubviewToFront:self.addressAdd_View];
                 [self fetchMerchantWithProductID:@"1015846"];
                 
            
            }

            break;
            
        default:
            
            break;
            
    }

    
}
//============================== OFFER , T&C AND STORES METHODS =======================//
- (void) cleanMainController {
    
    for (UIView *subView in mainContentHolder.subviews) {
        [subView removeFromSuperview];
    }
    
}
- (void) updateOfferView {
    
    if (!offerView) {
        return;
    }
    
    if ((offerView.subviews.count != 0) ) {
        offerTextView = (UITextView *)[offerView viewWithTag:2012];
        
    }
    else {
        NSLog(@"updateOfferView else");
        offerTextView = [[UITextView alloc] initWithFrame:offerView.bounds];
        offerTextView.tag = 2012;
        offerTextView.layer.cornerRadius = 4.0;
        
        productOfferDetailPlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(14, 7, 200, 30)];
        productOfferDetailPlaceHolder.text = @"Type Detailed Product Offer";
        productOfferDetailPlaceHolder.backgroundColor = [UIColor clearColor];
        productOfferDetailPlaceHolder.font = [UIFont systemFontOfSize:14];
        productOfferDetailPlaceHolder.textColor = [UIColor lightGrayColor];
        [offerTextView addSubview:productOfferDetailPlaceHolder];
    }
  
    if([routePath isEqualToString:@"productDetail"]){
        NSLog(@"updateOfferView productDetail");
        productOfferDetailPlaceHolder.hidden = YES;
        offerTextView.text = current_PostingProduct.productDetailedDesciption;
        
    }
    offerTextView.delegate = self;
    [offerView addSubview:offerTextView];
    
}

- (void) addOfferViewAsSubview {

    [mainContentHolder addSubview:self.offerView];
   
}

-(void)updateTcView
{
    if (!tcView) {
        return;
    }
    
    if ((tcView.subviews.count != 0) ) {
        tcTextView = (UITextView *)[tcView viewWithTag:2013];
        
    }
    else {
        tcTextView = [[UITextView alloc] initWithFrame:tcView.bounds];
        tcTextView.tag = 2013;
        tcTextView.layer.cornerRadius = 4.0;
        
        productTCPlaceHolder = [[UILabel alloc]initWithFrame:CGRectMake(14, 7, 200, 30)];
        productTCPlaceHolder.text = @"Type Terms and Conditions";
        productTCPlaceHolder.backgroundColor = [UIColor clearColor];
        productTCPlaceHolder.font = [UIFont systemFontOfSize:14];
        productTCPlaceHolder.textColor = [UIColor lightGrayColor];
        [tcTextView addSubview:productTCPlaceHolder];
        
    }
    if([routePath isEqualToString:@"productDetail"]){
        productTCPlaceHolder.hidden = YES;
        tcTextView.text = current_PostingProduct.termsAndConditions;
        
    }
    tcTextView.delegate = self;
    [tcView addSubview:tcTextView];
    
}

-(void)addTCViewAsSubview{
    
    [mainContentHolder addSubview:self.tcView];
}
-(IBAction)doneButton_Pressed:(id)sender
{
    
    NSLog(@"doneButton_Pressed");
    if(current_TextView == productName_TextView && current_TextView.text.length == 0)
        productNamePlaceHolder.hidden = NO;
    
    else if(current_TextView == productOffer_TextView && current_TextView.text.length == 0)
        productOfferPlaceHolder.hidden = NO;
    
    else if(current_TextView == offerTextView && current_TextView.text.length == 0)
        productOfferDetailPlaceHolder.hidden = NO;
    
    if(current_TextView == tcTextView &&  current_TextView.text.length == 0)
        productTCPlaceHolder.hidden = NO;
    
    
    [current_TextView resignFirstResponder];
}
- (IBAction) mapButtonPressed:(id) sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====");
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

- (IBAction)checkButton_Tapped:(UIButton *)checkButton
{
    
    
    if (checkButton.selected){
           [checkButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
           checkButton.selected = NO;
        
    }
    else{
        [checkButton setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
        checkButton.selected = YES;
        merchantAddressId = checkButton.tag;
    }
       
}


//================================= UITEXTVIEW DELEGATE METHODS =================================//

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
   // NSLog(@"TextViewbegin");
    
    if(textView == productName_TextView)
        productNamePlaceHolder.hidden = YES;
    
    else if(textView == productOffer_TextView)
        productOfferPlaceHolder.hidden = YES;
    
    else if(textView == offerTextView){
        
        productOfferDetailPlaceHolder.hidden= YES;
        [self setViewMovedUp:YES];}
    
    else if(textView == tcTextView){
        productTCPlaceHolder.hidden = YES;
        [self setViewMovedUp:YES];}
    
    else {}
    
    current_TextView = textView;
    [textView setInputAccessoryView:accessoryView];
    
    return YES;
    
}

-(void) textViewDidChange:(UITextView *)textView
{
      //NSLog(@"textViewDidChange");
    
    if(textView == productName_TextView && textView.text.length == 0)
    {
        
            productNamePlaceHolder.hidden = NO;
            [textView resignFirstResponder];
        
    }
    
    else if(textView == productOffer_TextView && textView.text.length == 0)
    {
       
        
            productOfferPlaceHolder.hidden = NO;
            [textView resignFirstResponder];
        
    }
    
    else if(textView == offerTextView && textView.text.length == 0){
        
        
            productOfferDetailPlaceHolder.hidden = NO;
            [textView resignFirstResponder];
            
        
    }
    else{
        if(textView.text.length == 0)
        {
            productTCPlaceHolder.hidden = NO;
            [textView resignFirstResponder];
            
        }}
    
}
//- (void) textViewDidChangeSelection:(UITextView *)textView
//{
//    NSLog(@"Fire change selection.");
//}


-(void)textViewDidEndEditing:(UITextView *)textView
{
     if(textView == offerTextView || textView == tcTextView)
            [self setViewMovedUp:NO];
}

//====================================UITEXTFIELD DELEGATE METHODS==================================//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn:");
    
    [textField resignFirstResponder];
    [self setViewMovedUp:NO];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   
       if(!isViewUp)
        [self setViewMovedUp:YES];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
        //[self setViewMovedUp:NO];
}

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        
        return;
    }
    
        
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,productId];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    merchantFetchRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}


//=========================== NSURLCONNECTION DELEGATE METHODS ==========================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
        [_responseData appendData:data];
    
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:data];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        
    
}
//===============--------PARSER DELEGATE METHODS-------===================//
- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    NSLog(@"\n MerchantslistCount in  parsingDataFinished method = %d",[merchantsListLocal count]);
    
    if (!merchantList) {
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    }
    else {
        [merchantList removeAllObjects];
        [merchantList addObjectsFromArray:merchantsListLocal];
    }
    
    [merchantTableView reloadData];
}

- (void)parsingMerchantListXMLFailed {
    
}

//=============================TABLEVIEW DELEGATE METHODS========================================//
#pragma mark -- UITableView Datasource & Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n MerchantsList count = %d",[merchantList count]);
    return [merchantList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == merchantList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    MerchantAddressCell *cell = (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    cell= (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
    if(cell==nil)
    {
        
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
//    NSLog(@"Address: %@",aMerchant.mMailAddress1);
//    NSLog(@"Suburb:%@",aMerchant.mMailSuburb);
//    NSLog(@"State:%@",aMerchant.mState);
//    NSLog(@"Phone:%@",aMerchant.mPhone);
    
    if([aMerchant.mMailAddress1 length]<=0 && [merchantList count] == 1){
        NSLog(@"null");
        cell.merchantAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str = aMerchant.mMailAddress1;
        
        if([aMerchant.mMailSuburb length]>0)
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        cell.merchantAddress.text = str;
        cell.mapButton.hidden = NO;
        cell.mapLabel.hidden = NO;
        cell.checkButton.hidden = NO;
    }
    cell.checkButton.tag = [aMerchant.mId intValue];
    [cell.mapButton addTarget:self action:@selector(mapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.checkButton addTarget:self action:@selector(checkButton_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.merchantAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    
    
    NSLog(@"%f,%f",Location.latitude,Location.longitude);
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}
-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}


//==============================================================================================================//
//===================================== UIPICKERVIEW DELEGATES METHODS =========================================//

#pragma mark -
#pragma mark Picker View Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
	if(pickerView == searchCriteriaPicker)
        return [searchCriteriaLabels_Array count];
    else if(pickerView == productsLeft_PickerView)
	   return [productLeftItems_Array count];
    else
       return [hoursCountDown_Array count];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	if(pickerView == searchCriteriaPicker)
        
	    return [searchCriteriaLabels_Array objectAtIndex:row];
    else if(pickerView == productsLeft_PickerView)
        return [productLeftItems_Array objectAtIndex:row];
    else
        return [hoursCountDown_Array objectAtIndex:row]; 
	
}  


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	
    if(pickerView == searchCriteriaPicker)
    {
        NSLog(@"searchCriteriaPicker..");
        Category *catObj = [appDelegate.categoryList_Array objectAtIndex:row];
        gridLabelValue = self.productCategory_Label.text = catObj.catName;
        productCatId = catObj.catId;
        NSLog(@"productCatId::%@",productCatId);
    }
    else if(pickerView == productsLeft_PickerView){
        UILabel *label = [productLeftItems_Array objectAtIndex:row];
        
        productLeftCountStr = label.text;
    }
    else{
        UILabel *label = [hoursCountDown_Array objectAtIndex:row];
        hoursCountDownStr = label.text;
    }
}
//==================================================================================================================//
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
