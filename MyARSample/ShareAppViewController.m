//
//  ShareAppViewController.m
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ShareAppViewController.h"

@interface ShareAppViewController ()

@end

@implementation ShareAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Share this App";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   [self.navigationController.navigationBar setTranslucent:NO];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
}

- (IBAction)facebookButon_Tapped:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
       
        [controller setInitialText:@"LastMinute Deals"];
        //[controller addImage:self.imageView.image];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (IBAction)twitterButton_Tapped:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
       
        [tweetSheet setInitialText:@"LastMinute Deals"];
        //[tweetSheet addImage:self.imageView.image];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)emailButton_Tapped:(id)sender {
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
        composer.mailComposeDelegate = self;
        [composer setSubject:@"Use this app for Deals"];
        
        NSString *emailBody = [NSString stringWithFormat:@""];
        [composer setMessageBody:emailBody isHTML:NO];
        
        NSArray *arr=[[NSArray alloc]initWithObjects:@"prashanthanits@yahoo.com", nil];
        [composer setToRecipients:arr];
        
        [self presentViewController:composer animated:YES completion:nil];
    }
    else
    {
        // Show some error message here
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        return;
    }
    

}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
