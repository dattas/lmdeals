//
//  PostADealMenuViewController.h
//  MyARSample
//
//  Created by vairat on 13/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataXMLParser.h"
#import "ProductListParser.h"
@interface PostADealMenuViewController : UIViewController <NSURLConnectionDelegate, UITextFieldDelegate, NSXMLParserDelegate, UserDataXMLParser, ProductListXMLParserDelegate>{
    
    NSMutableData *_responseData;
    
}
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIButton *menuButton;



@property (strong, nonatomic) IBOutlet UIView *loginContainerView;
@property (strong, nonatomic) IBOutlet UIView *loginContainer_Container;
@property (strong, nonatomic) IBOutlet UIView *menuView;

- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)postADealButtonTapped:(id)sender;
- (IBAction)menuButton_Action:(id)sender;
@end
