//
//  MyDealsViewController.h
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDealsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
}
@property (nonatomic, readwrite) IBOutlet UILabel *noProductsFoundLabel;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) NSMutableArray *productsList;
@end
