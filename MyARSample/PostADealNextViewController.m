//
//  PostADealNextViewController.m
//  MyARSample
//
//  Created by vairat on 18/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PostADealNextViewController.h"
#import "PostADealCell.h"
#import <QuartzCore/QuartzCore.h>

@interface PostADealNextViewController (){
    
    NSArray *cellImages_Array;
    NSArray *cellTitles_Array;
    
    NSURLConnection *postADealConnection;
    NSURLConnection *postADealImgConn;
    NSString *routePath;
    UIButton *myButton;
    NSMutableArray *switchArray;
    NSMutableArray *buttonArray;
    
    UILabel *label;
    
    
   }
-(void)switchAction:(id)sender;
@end

@implementation PostADealNextViewController
@synthesize myTableView;
@synthesize myTextField;
@synthesize accessoryView;
@synthesize currentTextField;
@synthesize contactTextField;
@synthesize curr_PostingProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil postProduct:(Product *)pro throughView:(NSString *)through
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.curr_PostingProduct = pro;
       routePath = through;
        cellImages_Array = [[NSArray alloc]initWithObjects:@"fb_white3.png",@"twitter_white2.png",@"email_white2.png",@"phone_white3.png",@"booknow_white3.png",@"website_white3.png",@"comment_white2.png",@"score_white2.png",@"redeemQR_white3.png",@"Redeem_white.png",nil];
        cellTitles_Array = [[NSArray alloc]initWithObjects:@"Facebook",@"Twitter",@"Email",@"Phone",@"Booknow",@"Website",@"Comment",@"Score",@"Scan",@"Redeem", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Submit"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(submitButtonTapped:)];
    
    switchArray = [[NSMutableArray alloc]init];
    buttonArray = [[NSMutableArray alloc]init];
    
    //to create UISwitchs and UIButtons
    for (int i =0; i<10; i++) {
        
        UISwitch *mySwitch = [[UISwitch alloc]initWithFrame:CGRectMake(7,16,75,25)];
        mySwitch.tag = i;
        mySwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
        [mySwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        [switchArray addObject:mySwitch];
        
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(260, 5, 50, 50)];
        button.tag = i;
        [button setImage:[UIImage imageNamed:[cellImages_Array objectAtIndex:i]] forState:UIControlStateNormal];
        [button  addTarget:self action:@selector(buttonPressedAction:) forControlEvents:UIControlEventTouchUpInside];
        [buttonArray addObject:button];
        
    }
    
  
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
   [self.navigationController.navigationBar setTranslucent:NO];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
}

-(void)submitButtonTapped: (id)sender
{
    NSLog(@"submitButtonTapped.....");
    
    curr_PostingProduct.merchantId = @"13064";
    NSString *temp = @"1";
   // NSString *currentpid = @"61";
    NSString *urlString;
    
    
  //  NSLog(@"curr_PostingProduct.productId::%@",curr_PostingProduct.productId);
  //  NSLog(@"curr_PostingProduct.product_CategoryId::%@",curr_PostingProduct.product_CategoryId);
    
    
    NSString *fbUrl = [curr_PostingProduct.product_FbUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *twtUrl = [curr_PostingProduct.product_TwitterUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *emailAddress = [curr_PostingProduct.product_EmailAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *contact1 = [curr_PostingProduct.product_PhoneNo1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *booknowText = [curr_PostingProduct.product_BookNowText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *websiteUrl = [curr_PostingProduct.product_WebsiteUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *commenttext = [curr_PostingProduct.product_CommentText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *scoreText = [curr_PostingProduct.product_ScoreText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *scanText = [curr_PostingProduct.product_ScoreText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *redeemText = [curr_PostingProduct.product_RedeemText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    
    
    if ([routePath isEqualToString:@"Menu"]) {
        
    
        NSLog(@"Menu");
/*    urlString = [NSString stringWithFormat:@"%@post_deal.php?m_id=%@&logo=%@&name=%@&hg=%@&count=%d&cat_id=%@&hd=%d&sd=%@&ed=%@&od=%@&tc=%@&stores=%d&fbs=%d&fbu=%@&tws=%d&twu=%@&es=%d&ea=%@&pns=%d&pn1=%@&pn2=%@&bs=%d&bt=%@&ws=%d&wu=%@&cs=%d&ct=%@&scs=%d&sct=%@&sts=%d&stt=%@&rs=%d&rt=%@&active=%d",URL_Prefix,curr_PostingProduct.merchantId,temp,curr_PostingProduct.productName,curr_PostingProduct.productOffer,curr_PostingProduct.product_AvailableCount,curr_PostingProduct.product_CategoryId,curr_PostingProduct.product_HoursCountDown,curr_PostingProduct.product_StartDate,curr_PostingProduct.product_EndDate,curr_PostingProduct.productDetailedDesciption,curr_PostingProduct.termsAndConditions,curr_PostingProduct.StoreId,curr_PostingProduct.product_FbStatus,curr_PostingProduct.product_FbUrl,curr_PostingProduct.product_TwitterStatus,curr_PostingProduct.product_TwitterUrl,curr_PostingProduct.product_EmailStatus,curr_PostingProduct.product_EmailAddress,curr_PostingProduct.product_PhoneNoStatus,curr_PostingProduct.product_PhoneNo1,curr_PostingProduct.product_PhoneNo2,curr_PostingProduct.product_BookNowStatus,curr_PostingProduct.product_BookNowText,curr_PostingProduct.product_WebsiteStatus,curr_PostingProduct.product_WebsiteUrl,curr_PostingProduct.product_CommentStatus,curr_PostingProduct.product_CommentText,curr_PostingProduct.product_ScoreStatus,curr_PostingProduct.product_ScoreText,curr_PostingProduct.product_ScanStatus,curr_PostingProduct.product_ScanText,curr_PostingProduct.product_RedeemStatus,curr_PostingProduct.product_RedeemText,curr_PostingProduct.product_Active];  */
        
        urlString = [NSString stringWithFormat:@"%@post_deal.php?m_id=%@&logo=%@&name=%@&hg=%@&count=%d&cat_id=%@&hd=%d&sd=%@&ed=%@&od=%@&tc=%@&stores=%d&fbs=%d&fbu=%@&tws=%d&twu=%@&es=%d&ea=%@&pns=%d&pn1=%@&pn2=%@&bs=%d&bt=%@&ws=%d&wu=%@&cs=%d&ct=%@&scs=%d&sct=%@&sts=%d&stt=%@&rs=%d&rt=%@&active=%d",URL_Prefix,curr_PostingProduct.merchantId,temp,curr_PostingProduct.productName,curr_PostingProduct.productOffer,curr_PostingProduct.product_AvailableCount,curr_PostingProduct.product_CategoryId,curr_PostingProduct.product_HoursCountDown,curr_PostingProduct.product_StartDate,curr_PostingProduct.product_EndDate,curr_PostingProduct.productDetailedDesciption,curr_PostingProduct.termsAndConditions,curr_PostingProduct.StoreId,curr_PostingProduct.product_FbStatus,fbUrl,curr_PostingProduct.product_TwitterStatus,twtUrl,curr_PostingProduct.product_EmailStatus,emailAddress,curr_PostingProduct.product_PhoneNoStatus,contact1,curr_PostingProduct.product_PhoneNo2,curr_PostingProduct.product_BookNowStatus,booknowText,curr_PostingProduct.product_WebsiteStatus,websiteUrl,curr_PostingProduct.product_CommentStatus,commenttext,curr_PostingProduct.product_ScoreStatus,scoreText,curr_PostingProduct.product_ScanStatus,scanText,curr_PostingProduct.product_RedeemStatus,redeemText,curr_PostingProduct.product_Active];
    }
    else{
        NSLog(@"Edit");
        urlString = [NSString stringWithFormat:@"%@update_deal.php?id=%@&m_id=%@&logo=%@&name=%@&hg=%@&count=%d&cat_id=%@&hd=%d&sd=%@&ed=%@&od=%@&tc=%@&stores=%d&fbs=%d&fbu=%@&tws=%d&twu=%@&es=%d&ea=%@&pns=%d&pn1=%@&pn2=%@&bs=%d&bt=%@&ws=%d&wu=%@&cs=%d&ct=%@&scs=%d&sct=%@&sts=%d&stt=%@&rs=%d&rt=%@&active=%d",URL_Prefix,curr_PostingProduct.productId,curr_PostingProduct.merchantId,temp,curr_PostingProduct.productName,curr_PostingProduct.productOffer,curr_PostingProduct.product_AvailableCount,curr_PostingProduct.product_CategoryId,curr_PostingProduct.product_HoursCountDown,curr_PostingProduct.product_StartDate,curr_PostingProduct.product_EndDate,curr_PostingProduct.productDetailedDesciption,curr_PostingProduct.termsAndConditions,curr_PostingProduct.StoreId,curr_PostingProduct.product_FbStatus,curr_PostingProduct.product_FbUrl,curr_PostingProduct.product_TwitterStatus,curr_PostingProduct.product_TwitterUrl,curr_PostingProduct.product_EmailStatus,curr_PostingProduct.product_EmailAddress,curr_PostingProduct.product_PhoneNoStatus,curr_PostingProduct.product_PhoneNo1,curr_PostingProduct.product_PhoneNo2,curr_PostingProduct.product_BookNowStatus,curr_PostingProduct.product_BookNowText,curr_PostingProduct.product_WebsiteStatus,curr_PostingProduct.product_WebsiteUrl,curr_PostingProduct.product_CommentStatus,curr_PostingProduct.product_CommentText,curr_PostingProduct.product_ScoreStatus,curr_PostingProduct.product_ScoreText,curr_PostingProduct.product_ScanStatus,curr_PostingProduct.product_ScanText,curr_PostingProduct.product_RedeemStatus,curr_PostingProduct.product_RedeemText,curr_PostingProduct.product_Active];
        
    }
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    postADealConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    NSString *str = [url absoluteString];
    
    
    NSLog(@"======>>URL::%@",str);
    
    
    
        
    
}


//=======================------------- NSURLCONNECTION METHODS ----------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    NSLog(@"RESPONSE::::%@",responseString);
    
    
    //if ([responseString isEqualToString:@"success"]) {
    /*    NSData *imageData = UIImagePNGRepresentation(curr_PostingProduct.productImage);
       
        
        NSMutableURLRequest *imgRequest = [[NSMutableURLRequest alloc] init];
        [imgRequest setHTTPMethod:@"POST"];
        [imgRequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@uploader.php",URL_Prefix]]];
        NSLog(@"path::%@",[NSString stringWithFormat:@"%@uploader.php",URL_Prefix]);
       [imgRequest appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",filename]] dataUsingEncoding:NSUTF8StringEncoding]];        [imgRequest setHTTPBody:imageData];
        
        
        postADealImgConn = [[NSURLConnection alloc] initWithRequest:imgRequest delegate:self];   */
        
     /*
        NSString *filename = @"myImage.png";
        NSData *imageData = UIImagePNGRepresentation(curr_PostingProduct.productImage);
        
        if(imageData){
            NSLog(@"have image data");
        }
        
        
        NSString *urlString = [NSString stringWithFormat:@"%@uploader.php",URL_Prefix];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",filename]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSLog(@"======%@",returnString);
        
        
        
   // }  */
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"ERROR:");
}



- (BOOL)uploadImage:(NSData *)imageData filename:(NSString *)filename{
    
    
    NSString *urlString = @"http://www.yourdomainName.com/yourPHPPage.php";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n",filename]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    return ([returnString isEqualToString:@"OK"]);
}

//=====================------------------ PARSING METHODS --------------=============================//






//==================------------------- TABLEVIEW DELEGATE METHODS --------------=============================//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"Cell";
    PostADealCell *cell = (PostADealCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PostADealCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
  
    if(indexPath.row == 0)
        myButton = cell.cellButton;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellLabel.text = [cellTitles_Array objectAtIndex:indexPath.row];
    cell.cellLabel.textColor=[UIColor blackColor];
    [cell.contentView addSubview:[switchArray objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:[buttonArray objectAtIndex:indexPath.row]];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
      return 60;
}


- (void)buttonPressedAction:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    CGRect frame =[self.view.window convertRect:self.view.window.frame
                                       fromView:button];
    
    flipViewController = [[FlipViewController alloc]initWithNibName:@"FlipViewController" bundle:nil];
    flipViewController.closeButtonTag = [sender tag];
    
    UIView *toview = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0,300, 300)];
    toview.clipsToBounds = NO;
    toview.autoresizesSubviews = YES;
    
    [[toview layer] setShadowOffset:CGSizeMake(4, 4)];
    [[toview layer] setShadowRadius:4];
    [[toview layer] setShadowOpacity:1.0];
    [[toview layer] setShadowColor:[UIColor blackColor].CGColor];
    toview.layer.cornerRadius = 10;
    toview.layer.borderWidth = 3.0;
    toview.layer.borderColor = [UIColor blackColor].CGColor;

    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button1.frame = CGRectMake(50,240,200,40);
    [button1 setTitle:@"Close" forState:UIControlStateNormal];
    [toview addSubview:button1];    // to add items
    [button1 addTarget:flipViewController action:@selector(viewDismiss:)  forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    saveButton.frame = CGRectMake(50,180,200,40);
    saveButton.tag = [sender tag];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [toview addSubview:saveButton];    // to add items
    [saveButton addTarget:flipViewController action:@selector(saveButton_Clicked:)  forControlEvents:UIControlEventTouchUpInside];
    
    toview.backgroundColor = [UIColor whiteColor];
    
   //=========
    
    
    
    switch ([sender tag]) {
        case 0:
               myTextField = [self getTextField];
               myTextField.tag = [sender tag];
               myTextField.placeholder = @"Enter Facebook Url";
                if ([curr_PostingProduct.product_FbUrl length] > 0) 
                     myTextField.text = curr_PostingProduct.product_FbUrl;
               [toview addSubview:myTextField]; 
               break;
        case 1:
              myTextField = [self getTextField];
              myTextField.tag = [sender tag];
              myTextField.placeholder = @"Enter Twitter Url";
                if ([curr_PostingProduct.product_TwitterUrl length] > 0)
                     myTextField.text = curr_PostingProduct.product_TwitterUrl;
              [toview addSubview:myTextField]; 
              break;
            
        case 2:
               myTextField = [self getTextField];
               myTextField.tag = [sender tag];
               myTextField.placeholder = @"Enter Email Address";
                if ([curr_PostingProduct.product_EmailAddress length] > 0)
                     myTextField.text = curr_PostingProduct.product_EmailAddress;
               [toview addSubview:myTextField]; 
               break;
            
        case 3:
               myTextField = [self getTextField];
               myTextField.tag = [sender tag];
               myTextField.placeholder = @"Enter Contact Number1";
               myTextField.keyboardType = UIKeyboardTypeNumberPad;
                 if ([curr_PostingProduct.product_PhoneNo1 length] > 0)
                      myTextField.text = curr_PostingProduct.product_PhoneNo1;
            
               contactTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0, 125.0, 200.0, 25.0)];
               contactTextField.font = [UIFont systemFontOfSize:14];
               contactTextField.keyboardType = UIKeyboardTypeNumberPad;
               contactTextField.borderStyle = UITextBorderStyleRoundedRect;
               contactTextField.delegate = self;
               contactTextField.placeholder = @"Enter Contact Number2";
               contactTextField.tag = [sender tag];
               [contactTextField setBackgroundColor:[UIColor whiteColor]];
                if ([curr_PostingProduct.product_PhoneNo2 length] > 0)
                     contactTextField.text = curr_PostingProduct.product_PhoneNo2;
               [toview addSubview:myTextField]; 
               [toview addSubview:contactTextField];
               break;
            
        case 4:{
               UITextView *mytextView = [self getTextView];
               mytextView.tag = [sender tag];
            
               [toview addSubview:mytextView];
               break;
              }
        case 5:
               myTextField = [self getTextField];
               myTextField.tag = [sender tag];
               myTextField.placeholder = @"Website Url";
                if ([curr_PostingProduct.product_WebsiteUrl length] > 0)
                     myTextField.text = curr_PostingProduct.product_WebsiteUrl;
               [toview addSubview:myTextField];
               break;
        case 6:
              {
               UITextView *mytextView = [self getTextView];
               mytextView.tag = [sender tag];
            
               [toview addSubview:mytextView];
              }
        case 7:
              myTextField = [self getTextField];
              myTextField.tag = [sender tag];
              myTextField.placeholder = @"Score";
              if ([curr_PostingProduct.product_ScoreText length] > 0)
                   myTextField.text = curr_PostingProduct.product_ScoreText;
              [toview addSubview:myTextField];
               break;
            
        case 8:
            myTextField = [self getTextField];
            myTextField.tag = [sender tag];
            myTextField.placeholder = @"Scan";
            if ([curr_PostingProduct.product_ScanText length] > 0)
                myTextField.text = curr_PostingProduct.product_ScanText;
            [toview addSubview:myTextField];
             break;
            
        case 9:
            myTextField = [self getTextField];
            myTextField.tag = [sender tag];
            myTextField.placeholder = @"Redeem";
            if ([curr_PostingProduct.product_RedeemText length] > 0)
                myTextField.text = curr_PostingProduct.product_RedeemText;
            [toview addSubview:myTextField];
             break;
        default:
                break;
    }
    
    
    
     
   //========
    [flipViewController setFromView:button toView:toview withX:frame.origin.x withY:frame.origin.y];
    [flipViewController startFirstAnimation];
    flipViewController.delegate = self;
    [self.view.window addSubview:flipViewController.view];
    
    
    
    
    
}

-(UITextField *)getTextField{
    
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(50.0, 85.0, 200.0, 25.0)];
    textField.font = [UIFont systemFontOfSize:14];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.delegate = self;
    [textField setBackgroundColor:[UIColor whiteColor]];
    
    return textField;
}

-(UITextView *)getTextView{
    
    UITextView *textView =[[UITextView alloc] initWithFrame:CGRectMake(50.0, 50.0, 200.0, 120.0)];
    label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0,textView.frame.size.width - 10.0, 34.0)];
    
    [label setText:@"Book now"];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor lightGrayColor]];
    [textView addSubview:label];
    [textView setBackgroundColor:[UIColor blueColor]];
    textView.delegate = self;
    
    return textView;

}



-(void)switchAction:(UISwitch *)mySwitch{
    NSLog(@"switchAction.tag::%d",[mySwitch tag]);
    
    switch ([mySwitch tag]) {
        case 0:
             if([mySwitch isOn]){
                curr_PostingProduct.product_FbStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
            }
              else
                curr_PostingProduct.product_FbStatus = 0;
                break;
        case 1:
             if([mySwitch isOn]){
                curr_PostingProduct.product_TwitterStatus = 1;
               [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
            }
              else
                curr_PostingProduct.product_TwitterStatus = 0;
                break;
        case 2:
             if([mySwitch isOn]){
                curr_PostingProduct.product_EmailStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
            }
              else
                curr_PostingProduct.product_EmailStatus = 0;
                break;
        case 3:
             if([mySwitch isOn]){
                curr_PostingProduct.product_PhoneNoStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
             }
              else
                curr_PostingProduct.product_PhoneNoStatus = 0;
                break;
        case 4:
             if([mySwitch isOn]){
                curr_PostingProduct.product_BookNowStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
             }
              else
                curr_PostingProduct.product_BookNowStatus = 0;
                break;
        case 5:
             if([mySwitch isOn]){
                curr_PostingProduct.product_WebsiteStatus = 1;
                 [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
             }
              else
                curr_PostingProduct.product_WebsiteStatus = 0;
                break;
        case 6:
             if([mySwitch isOn]){
                curr_PostingProduct.product_CommentStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
                }
              else
                curr_PostingProduct.product_CommentStatus = 0;
                break;
        case 7:
             if([mySwitch isOn]){
                curr_PostingProduct.product_ScoreStatus = 1;
                 [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
                }
              else
                curr_PostingProduct.product_ScoreStatus = 0;
                break;
        case 8:
            if([mySwitch isOn]){
                curr_PostingProduct.product_ScanStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
              }
              else
                curr_PostingProduct.product_ScanStatus = 0;
                break;
        case 9:
            if([mySwitch isOn]){
                curr_PostingProduct.product_RedeemStatus = 1;
                [self buttonPressedAction:[buttonArray objectAtIndex:[mySwitch tag]]];
              }
              else
                curr_PostingProduct.product_RedeemStatus = 0;
                break;
        default:
            break;
    }
    
    
}



-(void)closeButtontapped:(int)sender{
    
    UISwitch *s = [switchArray objectAtIndex:sender];
    
    
    
    switch (sender) {
        case 0:
               if ([curr_PostingProduct.product_FbUrl length] <=0)
                  [s setOn:NO animated:YES]; 
               break;
        case 1:
               if ([curr_PostingProduct.product_TwitterUrl length] <=0)
                   [s setOn:NO animated:YES];
               break;
        case 2:
               if ([curr_PostingProduct.product_EmailAddress length] <=0)
                   [s setOn:NO animated:YES];
               break;
        case 3:
               if ([curr_PostingProduct.product_PhoneNo1 length] <=0)
                   [s setOn:NO animated:YES];
               break;
        case 4:
              if ([curr_PostingProduct.product_BookNowText length] <=0)
                  [s setOn:NO animated:YES];
              break;
        case 5:
              if ([curr_PostingProduct.product_WebsiteUrl length] <=0)
                  [s setOn:NO animated:YES];
              break;
        case 6:
              if ([curr_PostingProduct.product_CommentText length] <=0)
                  [s setOn:NO animated:YES];
              break;
        case 7:
              if ([curr_PostingProduct.product_ScoreText length] <=0)
                  [s setOn:NO animated:YES];
               break;
        case 8:
             if ([curr_PostingProduct.product_ScanText length] <=0)
                 [s setOn:NO animated:YES];
              break;
        case 9:
             if ([curr_PostingProduct.product_RedeemText length] <=0)
                 [s setOn:NO animated:YES];
            break;
        default:
            break;
    }

    
    
    
}


-(void)savingFlipTextFieldData:(int)viewTag{
    
    
    NSLog(@"viewTag:::%d",viewTag);
    
    switch (viewTag) {
        case 0:
              curr_PostingProduct.product_FbUrl         = currentTextField.text;
              break;
        case 1:
              curr_PostingProduct.product_TwitterUrl    = currentTextField.text;
              break;
        case 2:
              curr_PostingProduct.product_EmailAddress  = currentTextField.text;
              break;
        case 3:
              curr_PostingProduct.product_PhoneNo1      = currentTextField.text;
              break;
        case 4:
              curr_PostingProduct.product_BookNowText   = currentTextField.text;
              break;
        case 5:
              curr_PostingProduct.product_WebsiteUrl    = currentTextField.text;
              break;
        case 6:
              curr_PostingProduct.product_CommentText   = currentTextField.text;
              break;
        case 7:
              curr_PostingProduct.product_ScoreText     = currentTextField.text;
              break;
        case 8:
              curr_PostingProduct.product_ScanText      = currentTextField.text;
              break;
        case 9:
              curr_PostingProduct.product_RedeemText    = currentTextField.text;
              break;
        default:
              break;
    }
    
    
    
}


-(void)formatDataForPostRequirements{
    
}

-(IBAction)doneButton_Pressed:(id)sender
{
    [self.currentTextField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    if(textField.tag == 3)
       [textField setInputAccessoryView:accessoryView];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    [textField resignFirstResponder];
    return  YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
