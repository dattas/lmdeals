//
//  AppDelegate.h
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RevealController.h"
#import "Product.h"
#import "User.h"

@class HomeViewController;
@class SearchCriteriaViewController;
@class HomePageViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeViewController *homeViewController;
@property (strong, nonatomic) SearchCriteriaViewController *searchCriteriaViewController;
@property (strong, nonatomic) HomePageViewController *homePageViewController;
@property (strong, nonatomic) NSMutableArray *productsList_Array;
@property (strong, nonatomic) NSMutableArray *categoryList_Array;
@property (nonatomic, strong) User *sessionUser;

- (BOOL) isIphone5;
- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord;

// My Favorite methods
- (BOOL) addProductToFavorites:(Product *) product;
- (BOOL) removeProductFromfavorites:(Product *) product;
- (BOOL) productExistsInFavorites:(Product *) product;
- (NSArray *) myFavoriteProducts;



// **** Core data properties
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end
