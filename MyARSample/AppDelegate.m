//
//  AppDelegate.m
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>

#import "HomeViewController.h"
#import "SearchCriteriaViewController.h"
#import "HomePageViewController.h"

#define Login_Token_Delimiter @""
#define Login_Token_Expiry_period (7*24*60*60)//1*60
#define Login_Token_Username_Key @"Login_Token_Username_Key"
#define Login_Token_Password_Key @"Login_Token_Password_Key"
#define Login_Token_Timestamp_Key @"Login_Token_Timestamp_Key"
#define User_object_Key @"User_object"

@implementation AppDelegate
@synthesize homeViewController;
@synthesize searchCriteriaViewController;
@synthesize homePageViewController;
@synthesize productsList_Array;
@synthesize categoryList_Array;
@synthesize sessionUser;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.productsList_Array = [[NSMutableArray alloc]init];
    self.categoryList_Array = [[NSMutableArray alloc]init];
    
    homeViewController = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    homePageViewController = [[HomePageViewController alloc]initWithNibName:@"HomePageViewController" bundle:nil];
    searchCriteriaViewController = [[SearchCriteriaViewController alloc]initWithNibName:@"SearchCriteriaViewController" bundle:nil];
    
    //UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homePageViewController];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:searchCriteriaViewController];
    
    self.window.rootViewController = revealController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL) isIphone5 {
    
    if( [ [ UIScreen mainScreen ] bounds ].size.height == 568.0){
        
        NSLog(@"====IPHONE5====");
        return YES;
    }
    NSLog(@"====IPHONE4====");
    return NO;
}



- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord{
    
    self.sessionUser = userDetails;
    
    NSDate *loginDate = [NSDate date];
    NSString *dateString = [loginDate description];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.username forKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] setValue:pWord forKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] setValue:dateString forKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
}

















#pragma mark -- Favorite methods

- (BOOL) addProductToFavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        // Product already exists. return.
        return NO;
    }
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *favoriteProduct = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [favoriteProduct setValue:product.productId forKey:@"productId"];
    [favoriteProduct setValue:product.productName forKey:@"productName"];
    [favoriteProduct setValue:product.productOffer forKey:@"productHighlight"];
   // [favoriteProduct setValue:self.sessionUser.userId forKey:@"userId"];
    [favoriteProduct setValue:@"1" forKey:@"userId"];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding product failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL) removeProductFromfavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // Fetch the said NSManagedObject(favoriteProduct)
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
        [fetchRequest setEntity:favProduct];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId == %@ && userId==1",product.productId];
        [fetchRequest setPredicate:predicate];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count > 0) {
            NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
            
            [context deleteObject:favoriteProduct];
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Removing product failed %@", [error localizedDescription]);
                
                // Removing object failed.
                return NO;
            }
            
            // Success
            return YES;
        }
        
        // There is no Product as such specified by input param in favorites.
    }
    
    // There is no Product as such specified by input param in favorites.
    return NO;
}



- (BOOL) productExistsInFavorites:(Product *) product {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId==%@",product.productId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
}

- (NSArray *) myFavoriteProducts {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==1"];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *productsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects) {
        NSLog(@"Product Name: %@", [info valueForKey:@"productName"]);
        NSLog(@"Product Id: %@", [info valueForKey:@"productId"]);
        
        Product *pr = [[Product alloc] init];
        pr.productId = [[info valueForKey:@"productId"] copy];
        pr.productName = [[info valueForKey:@"productName"] copy];
        pr.productOffer = [[info valueForKey:@"productHighlight"] copy];
        [productsArr addObject:pr];
    }
    
    return productsArr;
}

//======================================
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
/*
 - (NSManagedObjectModel *)managedObjectModel
 {
 if (_managedObjectModel != nil) {
 return _managedObjectModel;
 }
 NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FavoritesDataModel 2" withExtension:@"momd"];
 _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
 return _managedObjectModel;
 }
 */

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FavoritesDataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
