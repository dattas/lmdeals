//
//  ProductViewController.m
//  MyARSample
//
//  Created by vairat on 03/07/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "ProductViewController.h"
#import "AppDelegate.h"
#import "ProductCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MerchantAddressCell.h"

@interface ProductViewController (){
    Product *currentProduct;
    ProductDataParser *productDataXMLParser;
    NSString *productID;
    
    NSURLConnection *conn;
    NSURLConnection *proddImage;
    NSURLConnection *merchantFetchRequest;
    
    UIView *offerView;
    UIView *contactsView;
    UIView *addressView;
    
    MerchantListParser *merchantListParser;
    
    Product *thisProduct;
   
    AppDelegate *appDelegate;
    BOOL addressDetailsLoaded;
    UIView *loadingView;
}
@property(nonatomic,retain)NSURLConnection *conn;
@property(nonatomic,retain)NSURLConnection *proddImage;
@property(nonatomic,retain)NSURLConnection *merchantFetchRequest;
@property (nonatomic, strong) UIView *offerView;
@property (nonatomic, strong) UIView *contactsView;
@property (nonatomic, strong) UIView *addressView;
@property (nonatomic, strong) Product *thisProduct;
@end

@implementation ProductViewController


@synthesize couponMerchantImage;
@synthesize couponImageView;
@synthesize TandCButton;
@synthesize TandCView;
@synthesize TandCLabel;
@synthesize couponTandCTextView;
@synthesize TandCBackButton;
@synthesize couponFlipView;
@synthesize couponBgImageView;
@synthesize couponCardBackBg_ImageView;
@synthesize couponFrontView;
@synthesize couponBackView;
@synthesize redeemActivityIndicator;
@synthesize productImageView;
@synthesize scroller;
@synthesize offerView;
@synthesize contactsView;
@synthesize addressView;
@synthesize containerView;
@synthesize activityView;
@synthesize thisProduct;
@synthesize couponCardBg_ImageView;
@synthesize imgContainerView;
@synthesize conn;
@synthesize Location;
@synthesize proddImage;
@synthesize merchantFetchRequest;



@synthesize imageActivityIndicator;


@synthesize imageView;
@synthesize favButton;
@synthesize redeemButton;
@synthesize couponButton;
@synthesize productNameLabel;
@synthesize productOfferLabel;
@synthesize frontRedeemButton;

@synthesize mainContentHolder;
@synthesize offerButton;
@synthesize contactButton;
@synthesize addressButton;
@synthesize noImageLabel;
@synthesize couponView;
@synthesize discountLabel;
@synthesize memNameLabel,memNumberLabel,clientLabel;
@synthesize merchantTableView,mailComposeView;
@synthesize myCouponImage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productID:(NSString *)ProId 
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        productID = ProId;
        addressDetailsLoaded = NO;
        self.myCouponImage = nil;
    }
    return self;
}

- (IBAction)dissmissAction:(id)sender {
    
    NSLog(@"dissmissAction");
    [self dismissModalViewControllerAnimated:YES];
    
}

//============================ SHOW ALERTVIEW METHODS  =============================//
- (void) showActivityView {
    //activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}
- (void) dismissActivityView {
    [activityView removeFromSuperview];
}
//==================================================================================//


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"ViewDidLoad");
    self.title = @"Product Details";
    [self showActivityView];
 
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.myrewards.com.au/app/webroot/newapp/get_product.php?id=%@",productID]]];
    
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    offerButton.layer.borderColor = contactButton.layer.borderColor = addressButton.layer.borderColor = [UIColor orangeColor].CGColor;
    offerButton.layer.borderWidth = contactButton.layer.borderWidth = addressButton.layer.borderWidth = 2.0;
    couponButton.layer.borderColor = [UIColor orangeColor].CGColor;
    couponButton.layer.borderWidth = 2.0;
    
    imgContainerView.layer.cornerRadius = 5.0;
    imgContainerView.layer.borderColor = [UIColor orangeColor].CGColor;
    imgContainerView.layer.borderWidth = 2.0;

   
    
    couponMerchantImage.layer.cornerRadius = 5.0;
    couponMerchantImage.layer.borderColor = [UIColor orangeColor].CGColor;
    couponMerchantImage.layer.borderWidth = 2.0;
    
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor orangeColor].CGColor;
    imageView.layer.cornerRadius = 4.0;
    
     [self updateOfferView];


}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"viewWillAppear");
           [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
      [self.navigationController.navigationBar setTranslucent:NO];
    

        
        //============== Adding button to cell to enable back action
        
    /*    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(0,0, 320.0, 64.0);
        [myButton addTarget:self
                     action:@selector(dissmissAction:)
           forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:myButton];
        [self.view bringSubviewToFront:myButton];
        
        //==================
                // Update view size
        CGRect viewFrame = self.containerView.frame;
        viewFrame.origin.y = 64;
        viewFrame.size.height = viewFrame.size.height - 64;
        self.containerView.frame = viewFrame; */
        
        if (!couponView.hidden) {
            [self.view bringSubviewToFront:couponView];
        }
    
    
}




- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
        _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
   
    [_responseData appendData:data];
    
    NSLog(@"====>> didReceiveData");
    if(connection == conn)
    {
    
    NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:_responseData];
    productDataXMLParser = [[ProductDataParser alloc] init];
    productDataXMLParser.delegate = self;
    productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
  
    
    else if(connection == proddImage)
    {
        
            UIImage *image = [UIImage imageWithData:_responseData];
            [self.imageView setImage:image];
        self.myCouponImage = image;
        [self.couponMerchantImage setImage:image];
        
    }
    else if(connection == merchantFetchRequest) {
        
        NSLog(@"==merchantFetchRequest==");
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:data];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        addressDetailsLoaded = YES;
    }

    
}


- (void)parsingProductDataFinished:(Product *)product {
    
     NSLog(@"parsingProductDataFinished");
    
    currentProduct = product;
    thisProduct = product;
    
    // Create cell view
    ProductCell *cell = nil;
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
    
    for (UIView *cellview in views) {
        if([cellview isKindOfClass:[UITableViewCell class]]) {
            cell = (ProductCell*)cellview;
        }
    }
    
    // apply shadow
    cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    cell.headerContainerView.layer.shadowOpacity = 1.0;
    cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    cell.headerContainerView.layer.shadowRadius = 5.0;
    cell.headerContainerView.backgroundColor = [UIColor colorWithRed:244/255.0 green:123/255.0 blue:75/255.0 alpha:1.0];;
    
    // Update cell height
    CGRect cellFrame = cell.frame;
    cellFrame.origin.y = 0.0;
    cell.frame = cellFrame;
    
    
    [cell.closeButton addTarget:self action:@selector(dissmissAction:) forControlEvents:UIControlEventTouchUpInside];
        
    // Update view size
    CGRect viewFrame = self.containerView.frame;
    viewFrame.origin.y = cell.frame.size.height;
    viewFrame.size.height = viewFrame.size.height - cell.frame.size.height;
    self.containerView.frame = viewFrame;

    
    
    cell.productNameLabel.text = currentProduct.productName;
    cell.productOfferLabel.text = currentProduct.productOffer;
    NSLog(@"ViewWillAppear:: %@",currentProduct.productName);
    
    [self.view addSubview:cell];
    
    [self updateUIWithProductDetails:currentProduct];
    [self offerButtonPressed:offerButton];
    
    NSLog(@"productName::%@",currentProduct.productName);
    NSLog(@"productOffer::%@",currentProduct.productOffer);
    [self dismissActivityView];
    
    if (currentProduct.productImage)
    {
        
        [self.imageView setImage:currentProduct.productImage];
       
    }
    
    
    
    else
    {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:currentProduct.productImgLink]];
        proddImage = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    }

}
- (void) parsingProductDataXMLFailed{
    NSLog(@"XML failed");
}
//======================
- (IBAction) termsAndCondButtonPressed:(id)sender {
    
    NSLog(@"termsAndCondButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    NSLog(@"%@",thisProduct.productTermsAndConditions);
    couponTandCTextView.text = thisProduct.productTermsAndConditions;
    
    //couponMerchantImage.image = thisProduct.productImage;
    
    NSLog(@"::thisProduct.termsAndConditions=%@",thisProduct.productTermsAndConditions);
    couponFrontView.alpha = 0.0;
    couponBackView.alpha = 1.0;
    [UIView commitAnimations];
    
}

- (IBAction) termsAndCondBackButtonPressed:(id)sender {
    NSLog(@"termsAndCondBackButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 1.0;
    couponBackView.alpha = 0.0;
    [UIView commitAnimations];
}

- (void) couponGestureSelector:(UIGestureRecognizer *) gesRec {
    
    CGPoint touchPoint = [gesRec locationOfTouch:0 inView:couponView];
    
    if (couponFrontView.alpha == 0.0) {
        // i.e Front view is not visible, but backView is
        if( CGRectContainsPoint(TandCBackButton.frame, touchPoint) ) {
            [self termsAndCondBackButtonPressed:TandCBackButton];
        }
        else if( CGRectContainsPoint(redeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
    else {
        // i.e Back view is not visible, but frontView is
        if( CGRectContainsPoint(TandCButton.frame, touchPoint) ) {
            [self termsAndCondButtonPressed:TandCButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
}


-(IBAction)handleCouponTap:(id)sender
{
    NSLog(@"handleCouponTap:");
    couponView.hidden = YES;
    [self.containerView sendSubviewToBack:couponView];
}


- (void) addOfferViewAsSubview {
    
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = offerView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:self.offerView];
    
    [self updateScrollerContentSize];
    
}

- (void) addContactsViewAsSubview {
    
    contactsView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:contactsView];
    
    [self updateScrollerContentSize];
    
}

- (void) addAddressViewAsSubview {
    
    addressView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:addressView];
    
    [self updateScrollerContentSize];
    
}
- (void) updateScrollerContentSize {
    
    scroller.frame = self.containerView.bounds;
    
    float contentHeight = mainContentHolder.frame.origin.y + mainContentHolder.frame.size.height +10.0;
    
    scroller.contentSize = CGSizeMake(scroller.contentSize.width, contentHeight);
    
    NSLog(@"** scroller contentSize: %@",NSStringFromCGSize(scroller.contentSize));
    NSLog(@"** scroller frame: %@",NSStringFromCGRect(scroller.frame));
    
    scroller.scrollEnabled = YES;
    
}


- (void) updateOfferView {
    
//    if (!offerView) {
//        return;
//    }
    
    // Check if webview already exists
    
    UIWebView *offerWebView;
    //  offerWebView.scalesPageToFit = YES;
    
    if ((offerView.subviews.count != 0) ) {
        offerWebView = (UIWebView *)[offerView viewWithTag:2012];
        
    }
    else {
        offerWebView = [[UIWebView alloc] initWithFrame:offerView.bounds];
        
        NSLog(@"ELSE WEBVIEW");
    }
    
    NSString *webViewtext;
    if ([thisProduct.productDesciption length] <= 0 && [thisProduct.productText length] <= 0)
        webViewtext = @"";
    else
        webViewtext = [NSString stringWithFormat:@"%@%@",thisProduct.productDesciption,thisProduct.productText];
    
    NSString *myDescriptionHTML=[NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                                 "H4{ color: rgb(198,38,21) }\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "<body><H4>%@</H4>%@</body> \n"
                                 "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer, webViewtext];
    
    
    
    
    
    [offerWebView loadHTMLString:myDescriptionHTML baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    offerWebView.tag = 2012;
    //   offerWebView.scalesPageToFit = YES;
    //   offerWebView.multipleTouchEnabled= YES;
    offerWebView.delegate = self;
    [offerView addSubview:offerWebView];
}

- (IBAction) favoritesButtonPressed:(id) sender {
    

    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add to favorites?" message:[NSString stringWithFormat:@"Do you want to add %@ to favorites",thisProduct.productName] delegate:nil cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
           
           [alert show];
   
    
}

-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}

- (IBAction) offerButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!offerView) {
        offerView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        offerView.backgroundColor = [UIColor whiteColor];
        
        //offerView.layer.cornerRadius = 5.0;
        offerView.layer.borderColor = [UIColor orangeColor].CGColor;
        offerView.layer.borderWidth = 2.0;
    }
    
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = offerView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:self.offerView];
    
    NSLog(@"offerButtonPressed");
    [self cleanMainController];
    
    [self updateOfferView];
    
    [self addOfferViewAsSubview];
    [self updateUIWithProductDetails:thisProduct];
}
- (void) updateUIWithProductDetails:(Product *) aProduct {
    
        
    [self updateOfferView];
    
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex) {
        if (alertView.tag == 21) {
            // Add to fav case
           // BOOL success = [appDelegate addProductToFavorites:thisProduct];
            
          //  [self updateFavoriteStatus];
            
            
           // if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
             //   [delegate productAddedToFavorites:thisProduct];
           // }
            
        }
        else if (alertView.tag == 22) {
            // Remove from fav case
          //  BOOL success = [appDelegate removeProductFromfavorites:thisProduct];
            
          //  [self updateFavoriteStatus];
            
          //  if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
          //      [delegate productremovedFromFavorites:thisProduct];
          //  }
            
        }
    }
    else
    {
        if(alertView.tag == 111)
        {
            NSLog(@"call alert view and button index::%d",buttonIndex);
            if(buttonIndex == 1)
            {
                NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@",thisProduct.phone];
                NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
                [[UIApplication sharedApplication] openURL:phoneURL];
            }
        }
        if(alertView.tag == 222)
        {
            NSLog(@"Redeem alert view and button index::%d",buttonIndex);
            if(buttonIndex == 1)
            {
                
               // NSString *lattitude = @"0.00";   // [NSString stringWithFormat:@"%f",self.userLocation.latitude];
               // NSString *longitude = @"0.00";// [NSString stringWithFormat:@"%f",self.userLocation.longitude];
                // NSLog(@"%@",lattitude);
                // NSLog(@"%@",longitude);
                
//                NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
//                NSURL *url = [NSURL URLWithString:urlString];
//                NSLog(@"url is:==> %@",url);
//                
//                
//                redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
//                [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
//                [redeemRequest setPostValue:thisProduct.productId forKey:@"pid"];
//                [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
//                [redeemRequest setPostValue:lattitude forKey:@"lat"];
//                [redeemRequest setPostValue:longitude forKey:@"lon"];
//                
//                [redeemRequest setDelegate:self];
//                [redeemRequest startAsynchronous];
                [self.redeemActivityIndicator startAnimating];
                
                
                
            }
        }
        if(alertView.tag == 333)
        {
            NSLog(@"ThankYou alert view and button index::%d",buttonIndex);
            if(buttonIndex == 0)
            {
                
                [self handleCouponTap:nil];
                
            }
        }
    }//else
}

- (IBAction) redeemButtonPressed:(id) sender {
    
    //[delegate showMailComposer:nil withBody:nil];
    //[appDelegate.homeViewController showMailComposer:nil withBody:nil];
    
    
    
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use, so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
    [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    redemAlert.tag = 222;
    [redemAlert show];
    
    
    
}
- (IBAction) mapButtonPressed:(id) sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====");
    
    //  NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=newyork"];
    
    
    
   // NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    
   // [[UIApplication sharedApplication] openURL:addressUrl];
}
- (IBAction) contactButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!contactsView) {
        contactsView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        contactsView.backgroundColor = [UIColor whiteColor];
        
        //contactsView.layer.cornerRadius = 5.0;
        contactsView.layer.borderColor = [UIColor orangeColor].CGColor;
        contactsView.layer.borderWidth = 2.0;
        
        /*   UIButton *callButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
         [callButton addTarget:self
         action:@selector(callButtonpressed:)
         forControlEvents:UIControlEventTouchUpInside];
         
         callButton.frame = CGRectMake(30.0, 100.0, 25.0, 25.0);
         [contactsView addSubview:callButton];
         
         UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, theSize.width, theSize.height)];
         contactsLabel.text = thisProduct.phone;
         
         
         contactsLabel.numberOfLines = 0;
         contactsLabel.font = [UIFont systemFontOfSize:15.0];
         [contactsView addSubview:contactsLabel];*/
        
        
    }
    
    [self cleanMainController];
    [self updateContactsView];
    [self addContactsViewAsSubview];
    
}


- (IBAction) couponButtonPressed:(id) sender {
    
    
    
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponGestureSelector:)];
    recognizer.delegate = self;
    [couponView addGestureRecognizer:recognizer];
    
    couponView.frame = self.view.bounds;/*containerView.bounds*/;
    
    
    
    
    
    
    couponView.hidden = NO;
    
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
    // couponView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:couponView];
    
    discountLabel.text = [NSString stringWithFormat:@"%@",thisProduct.productOffer];
    productNameLabel.text = thisProduct.productName;
    //productOfferLabel.text = thisProduct.productOffer; //same as discount
    couponMerchantImage.image = self.myCouponImage; //thisProduct.productImage;
    
//    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
//    clientLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
//    memNumberLabel.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
    
    
    
    
    
    // Finalizing statements
    couponCardBg_ImageView.frame = couponFlipView.bounds;
    couponCardBackBg_ImageView.frame =  couponFlipView.bounds;
    couponBackView.frame = couponFlipView.bounds;
    couponFrontView.frame = couponFlipView.bounds;
    
    //======== configuring couponcard buttons and labels
    
    
    //buttons
    CGRect TandCButtonFrame = TandCButton.frame;
    TandCButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - TandCButtonFrame.size.width-22;
    if([appDelegate isIphone5])
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    TandCButton.frame = TandCButtonFrame;
    
    CGRect fRedeemButtonFrame = frontRedeemButton.frame;
    fRedeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - fRedeemButtonFrame.size.width-194;
    
    if([appDelegate isIphone5])
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    frontRedeemButton.frame = fRedeemButtonFrame;
    
    
    
    CGRect TandCBackButtonFrame = TandCBackButton.frame;
    TandCBackButtonFrame.origin.x = 40.0;
    TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-65;
    TandCBackButton.frame = TandCBackButtonFrame;
    
    CGRect redeemButtonFrame = redeemButton.frame;
    redeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - redeemButtonFrame.size.width - 23.0;
    redeemButtonFrame.origin.y = couponBackView.frame.size.height-65;
    redeemButton.frame = redeemButtonFrame;
    
    
    
    CGRect discountLabelFrame = discountLabel.frame;
    discountLabelFrame.origin.y = self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height+20;
    discountLabel.frame = discountLabelFrame;
    
    CGRect nameLabelFrame = memNameLabel.frame;
    if ([appDelegate isIphone5])
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height ;
    else
        nameLabelFrame.origin.y = 220;
    memNameLabel.frame = nameLabelFrame;
    
    CGRect memnumLabelFrame = memNumberLabel.frame;
    memnumLabelFrame.origin.y = self.memNameLabel.frame.origin.y + self.memNameLabel.frame.size.height;
    memNumberLabel.frame = memnumLabelFrame;
    
    CGRect clientLabelFrame = clientLabel.frame;
    clientLabelFrame.origin.y = self.memNumberLabel.frame.origin.y + self.memNumberLabel.frame.size.height;
    clientLabel.frame = clientLabelFrame;
    
    CGRect couponBgFrame = couponBgImageView.frame;
    if([appDelegate isIphone5])
        couponBgFrame.size.height = 170;
    else  couponBgFrame.size.height = 120;
    couponBgImageView.frame = couponBgFrame;
    
    
    
    
    //===========
    [couponFlipView addSubview:couponBackView];
    [couponFlipView addSubview:couponFrontView];
    couponBackView.alpha = 0.0;
    couponFrontView.alpha = 1.0;
    
    
       
}

- (IBAction) addressButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!addressView) {
        addressView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        addressView.backgroundColor = [UIColor whiteColor];
        
        //addressView.layer.cornerRadius = 5.0;
        addressView.layer.borderColor = [UIColor orangeColor].CGColor;
        addressView.layer.borderWidth = 2.0;
    }
    if(!merchantTableView)
    {
        merchantTableView = [[UITableView alloc]initWithFrame:mainContentHolder.bounds style:UITableViewStylePlain];
        merchantTableView.delegate = self;
        merchantTableView.dataSource = self;
        [addressView addSubview:merchantTableView];
    }
    
    [self cleanMainController];
    [self updateAddressView];
    [self addAddressViewAsSubview];
    
}

- (void) bringForwardTab:(UIButton *) tabBtn {
    
    if (tabBtn == offerButton) {
        
        //[scroller insertSubview:addressButton belowSubview:offerButton];
        //[scroller insertSubview:contactButton belowSubview:offerButton];
        
        offerButton.backgroundColor = [UIColor purpleColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale1.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
        
    }
    else if (tabBtn == contactButton) {
        //[scroller insertSubview:offerButton belowSubview:contactButton];
        //[scroller insertSubview:addressButton belowSubview:contactButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor purpleColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone1.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
    }
    else if (tabBtn == addressButton) {
        //[scroller insertSubview:offerButton belowSubview:addressButton];
        //[scroller insertSubview:contactButton belowSubview:addressButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor purpleColor];
        
        [offerButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop1.png"] forState:UIControlStateNormal];
    }
    
}

- (void) updateContactsView {
    NSLog(@"\n merchant name = %@", thisProduct.contactMerchantName);
    if (!contactsView) {
        return;
    }
    
    NSLog(@"phno::%@",thisProduct.phone);
    NSLog(@"website link::%@",thisProduct.websiteLink);
    phArray = [[NSMutableArray alloc]init];
    //===== if phnumber tag has Two numbers
    
    if([thisProduct.phone length]>15)
    {
        phnoCount = 2;
        NSLog(@"ph numbers Count:%d",phnoCount);
        if ([self doesString:thisProduct.phone containCharacter:'/'])// phNo having character '/'
        {
            
            NSArray *phNo = [thisProduct.phone componentsSeparatedByString:@"/"];
            // NSLog(@"phone No1::%@",[phNo objectAtIndex:0]);
            // NSLog(@"phone No2::%@",[phNo objectAtIndex:1]);
            
            NSString *ph1 = [phNo objectAtIndex:0];
            NSString *ph2 = [[ph1 substringToIndex:[ph1 length] - 3] substringFromIndex:0];
            // NSLog(@"ph2::%@",ph2);
            
            NSString *newString = [[phNo objectAtIndex:1]stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSMutableString *ph2formated = [[NSMutableString alloc]initWithString:ph2];
            [ph2formated appendString:newString];
            
            [phArray addObject:[phNo objectAtIndex:0]];
            [phArray addObject:ph2formated];
            
            
            // NSLog(@"ph2::%@",ph2formated);
        }
        
        else  // phNo having character ','
        {
            
            NSArray *myArray = [thisProduct.phone componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/, "]];
            //NSLog(@"phone No1::%@",[myArray objectAtIndex:0]);
            // NSLog(@"phone No2::%@",[myArray objectAtIndex:2]);
            
            NSArray *listItems = [thisProduct.phone componentsSeparatedByString:@"-"];
            NSString *code = [listItems objectAtIndex:0];
            // NSLog(@"code is ::%@",code);
            NSString *formatedPhNo = [NSString stringWithFormat:@"%@-%@",code,[myArray objectAtIndex:2]];
            // NSLog(@"phone No2::%@",formatedPhNo);
            
            [phArray addObject:[myArray objectAtIndex:0]];
            [phArray addObject:formatedPhNo];
        }
    }
    else
    {
        if([thisProduct.phone length] <= 0)
        {
            phnoCount = 0;
            NSLog(@"ph numbers Count:%d",phnoCount);}
        else{  phnoCount = 1;
            NSLog(@"ph numbers Count:%d",phnoCount);
            [phArray addObject:thisProduct.phone];
        }
    }
    //======
    if (phnoCount!=0) {
        
        
        NSMutableArray *contactsLblArray = [[NSMutableArray alloc]initWithCapacity:2];
        NSMutableArray *contactsLblBtnArray = [[NSMutableArray alloc]initWithCapacity:2];
        NSMutableArray *contactsBtnArray = [[NSMutableArray alloc]initWithCapacity:2];
        
        UILabel *contactlabel1 = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 30)];
        UILabel *contactlabel2 = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 150, 30)];
        
        UIButton *contactlabel1_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 5, 150, 30)];
        contactlabel1_Button.tag = 1;
        [contactlabel1_Button addTarget:self
                                 action:@selector(callButtonpressed:)
                       forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *contactlabel2_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 35, 150, 30)];
        contactlabel2_Button.tag = 2;
        [contactlabel2_Button addTarget:self
                                 action:@selector(callButtonpressed:)
                       forControlEvents:UIControlEventTouchUpInside];
        
        
        [contactsLblArray addObject:contactlabel1];
        [contactsLblArray addObject:contactlabel2];
        
        [contactsLblBtnArray addObject:contactlabel1_Button];
        [contactsLblBtnArray addObject:contactlabel2_Button];
        
        UIButton *contactButton1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        contactButton1.frame = CGRectMake(30.0,10.0, 25.0, 25.0);
        [contactButton1 setImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
        contactButton1.tag = 1;
        
        UIButton *contactButton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        contactButton2.frame = CGRectMake(30.0,40.0, 25.0, 25.0);
        [contactButton2 setImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
        contactButton2.tag = 2;
        [contactsBtnArray addObject:contactButton1];
        [contactsBtnArray addObject:contactButton2];
        
        for(int i =0;i<phnoCount;i++) {
            
            UILabel * label = [contactsLblArray objectAtIndex:i];
            label.text = [phArray objectAtIndex:i];
            label.numberOfLines = 0;
            label.font = [UIFont systemFontOfSize:15.0];
            [contactsView addSubview:label];
            [contactsView addSubview:[contactsLblBtnArray objectAtIndex:i]];
            
            UIButton *button = [contactsBtnArray objectAtIndex:i];
            
            [button addTarget:self
                       action:@selector(callButtonpressed:)
             forControlEvents:UIControlEventTouchUpInside];
            [contactsView addSubview:button];
            
        }
    }
    else{
        
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];
    }
    
    
    
    // website button and label
    if([thisProduct.websiteLink length] > 0)
    {
        UILabel *websiteLabel;
        UIButton *websiteLabel_Button;
        
        if([phArray count] ==1){
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];}
        else{
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60,65, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 65, 200, 30)];                                                                          }
        [websiteLabel_Button addTarget:self
                                action:@selector(websiteButtonpressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        
        websiteLabel.text = thisProduct.websiteLink;
        
        websiteLabel.numberOfLines = 0;
        websiteLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:websiteLabel];
        [contactsView addSubview:websiteLabel_Button];
        
        UIButton *websiteButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [websiteButton setImage:[UIImage imageNamed:@"globe.png"] forState:UIControlStateNormal];
        [websiteButton addTarget:self
                          action:@selector(websiteButtonpressed:)
                forControlEvents:UIControlEventTouchUpInside];
        if([phArray count] ==1)
            websiteButton.frame = CGRectMake(30.0,40.0, 25.0, 25.0);
        
        else
            websiteButton.frame = CGRectMake(30.0,70.0, 25.0, 25.0);
        [contactsView addSubview:websiteButton];
        
    }
    
}

- (void) cleanMainController {
    
    for (UIView *subView in mainContentHolder.subviews) {
        [subView removeFromSuperview];
    }
    
}

- (void) updateAddressView {
    
    [self fetchMerchantWithProductID:thisProduct.productId];
    
}

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    if(addressDetailsLoaded) {
        // Already address is processed.
        return;
    }
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,productId];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    merchantFetchRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(BOOL)doesString:(NSString *)string containCharacter:(char)character
{
    if ([string rangeOfString:[NSString stringWithFormat:@"%c",character]].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    NSLog(@"\n Merchants lIst in  parsingDataFinished metnod = %d",[merchantsListLocal count]);
    
    if (!merchantList) {
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    }
    else {
        [merchantList addObjectsFromArray:merchantsListLocal];
    }
    
    [merchantTableView reloadData];
}

- (void)parsingMerchantListXMLFailed {
    
}
#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n MerchantsList count = %d",[merchantList count]);
    return [merchantList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == merchantList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    MerchantAddressCell *cell = (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    /*  if (!cell) {
     
     NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:nil options:nil];
     
     for (UIView *cellview in views) {
     if([cellview isKindOfClass:[UITableViewCell class]])
     {
     cell = (MerchantAddressCell *)cellview;
     }
     }
     
     }*/
    
    
    cell= (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
    if(cell==nil)
    {
        
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    
    //    NSString *str;
    //
    //    if([thisProduct.contactSuburb length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactSuburb]];
    //    }
    //    if([thisProduct.contactState length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactState]];
    //    }
    //    if([thisProduct.contactPostcode length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactPostcode]];
    //    }
    //    if([thisProduct.contactCountry length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactCountry]];
    //    }
    //
    
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    NSLog(@"Address: %@",aMerchant.mMailAddress1);
    NSLog(@"Suburb:%@",aMerchant.mMailSuburb);
    NSLog(@"State:%@",aMerchant.mState);
    NSLog(@"Phone:%@",aMerchant.mPhone);
    
    if([aMerchant.mMailAddress1 length]<=0 && [merchantList count] == 1){
        NSLog(@"null");
        cell.merchantAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str = aMerchant.mMailAddress1;
        
        if([aMerchant.mMailSuburb length]>0)
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        cell.merchantAddress.text = str;
        cell.mapButton.hidden = NO;
        cell.mapLabel.hidden = NO;
    }
    cell.merchantAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    
    
    NSLog(@"%f,%f",Location.latitude,Location.longitude);
    // cell.productOfferLabel.text = aMerchant.mMailAddress1;
    // cell.productOfferLabel.textColor = [UIColor blackColor];
    // cell.imgLoadingIndicator.hidesWhenStopped = YES;
    
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}





//==============================================================================================================//
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
