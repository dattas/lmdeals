//
//  FlipViewController.m
//  MyARSample
//
//  Created by vairat on 03/10/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "FlipViewController.h"
#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

static CATransform3D CATransform3DMakePerspective(CGFloat z) {
    CATransform3D t = CATransform3DIdentity;
    t.m34 = - 1.0 / z;
    return t;
}

@interface FlipViewController (){
    
}
-(void)changeImgView:(UIImageView *)orgImage;
- (void)fullSrceen:(CALayer *)layer ;

@end

@implementation FlipViewController
@synthesize fromView,toView;
@synthesize delegate;
@synthesize closeButtonTag;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/*
-(void)setShowImage:(UIImage *)image withOrgImage:(UIImage *)orgImage withX:(float)x withY:(float)y
{
    NSLog(@"setShowImage");
   // [[UIApplication sharedApplication] setStatusBarHidden:YES];
    CALayer *layer = [CALayer layer];
          //  layer.anchorPoint = CGPointMake(0.0, 0.0);
    layer.frame = CGRectMake(x, y, image.size.width, image.size.height);
    layer.backgroundColor = [UIColor redColor].CGColor;
    layer.borderColor = [UIColor blackColor].CGColor;
    layer.opacity = 1.0f;
    layer.contents = (id)[image CGImage];
    [self.view.layer addSublayer:layer];
   // [self performSelector:@selector(changeImgView:)  withObject:imageView   afterDelay:0.3];
    float k =  orgImage.size.width/orgImage.size.height;
    //float k =  image.size.width/image.size.height;
    
    isHorizontal = (k>1);
    if (k <= 1) {
        //竖屏的图片放大
        if (k <= 32.0/48.0) {
            _width = 320;
            _height = _width/k;
        } else {
            _height = 480;
            _width = _height*k;
        }
    } else {
        //横屏的图片旋转
        if (k < 48.0/32.0) {
            _height = 480;
            _width = _height/k;
        } else {
            _width = 320;
            _height = _width*k;
        }
    }
    [self performSelector:@selector(fullSrceen:)  withObject:layer   afterDelay:0.1];
    
}

-(void)changeImgView:(UIImageView *)imagesView
{
    [self.view addSubview:imagesView];
    
}
- (void)fullSrceen:(CALayer *)layer
{
    CATransform3D transform;
    if(isHorizontal){
        transform = CATransform3DMakeRotation(-M_PI/2, 0.0,0.0,1.0);
    }else{
        transform = CATransform3DMakeRotation(0, 0.0,0.0,1.0);
    }
    layer.transform = transform;
    layer.frame =CGRectMake((320-_width)/2, (480-_height)/2, _width, _height);
    
    NSLog(@" [%f,%f]",_width,_height);
}
*/
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   [self.navigationController.navigationBar setTranslucent:NO];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
}

-(IBAction)viewDismiss:(id)sender
{
    NSLog(@"viewDismiss");
    fromView.layer.cornerRadius = 40;
    toView.layer.cornerRadius = 40;
    if (animationSyte == 1)
    {
        toView.center = cerr;
        fromView.center = cerr;
        [self.view bringSubviewToFront:toView];
        
        float direction = 0;
        if (translationX<0)
        {
            direction = -1;
        }
        
        [self.fromView.layer addAnimation:[self getAnimation:scaleX toScaleX:1.0 fromScaleY:scaleY tofromScaleY:1.0 fromTranslationX:translationX toTranslationX:0.0 fromTranslationY:translationY toTranslationY:0.0 fromTranslationZ:0.0 toTranslationZ:2.0 fromRotation:direction * 180.0 toRotation:0.0 removedOnCompletion:YES] forKey:@"endfromView"];
        
        [self.toView.layer addAnimation:[self getAnimation:1.0 toScaleX:1.0/scaleX fromScaleY:1.0 tofromScaleY:1.0/scaleY fromTranslationX:translationX toTranslationX:0.0 fromTranslationY:translationY toTranslationY:0.0 fromTranslationZ:0.0 toTranslationZ:1.0 fromRotation:direction * 360.0 toRotation:direction * 180.0 removedOnCompletion:NO] forKey:@"endtoView"];
        
        [opacityLayer addAnimation:[self getOpacityAnimation:0.5
                                                   toOpacity:0.0] forKey:@"opacity"];
        animationSyte = 2;
    }
    
}

- (void)dismiss
{
    NSLog(@"dismiss method");
    [self.view removeFromSuperview];
    [delegate closeButtontapped:closeButtonTag];
}

- (void)saveButton_Clicked:(id)sender{
    NSLog(@"saveButton_Clicked");
    
    [delegate savingFlipTextFieldData:[sender tag]];
    [self viewDismiss:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.layer.sublayerTransform = CATransform3DMakePerspective(1000);
 
}


- (void)startFirstAnimation
{
    
    
    float direction = 0;
    if (translationX<0)
    {
        direction = -1;
    }
    
    [self.fromView.layer addAnimation:[self getAnimation:1.0 toScaleX:scaleX fromScaleY:1.0 tofromScaleY:scaleY fromTranslationX:-translationX toTranslationX:0.0 fromTranslationY:-translationY toTranslationY:0.0 fromTranslationZ:0.0 toTranslationZ:1.0 fromRotation:0.0 toRotation:direction * 180.0 removedOnCompletion:YES] forKey:@"startfromView"];
    
    [self.toView.layer addAnimation:[self getAnimation:1.0/scaleX toScaleX:1.0 fromScaleY:1.0/scaleY tofromScaleY:1.0 fromTranslationX:-translationX toTranslationX:0.0 fromTranslationY:-translationY toTranslationY:0.0 fromTranslationZ:0.0 toTranslationZ:2.0 fromRotation:direction * 180.0 toRotation:direction * 360.0 removedOnCompletion:YES] forKey:@"starttoView"];

    [opacityLayer addAnimation:[self getOpacityAnimation:0.0
                                               toOpacity:0.5] forKey:@"opacity"];
    animationSyte = 0;
}

- (void)setFromView:(UIView *)fView toView:(UIView *)tView withX:(float)x withY:(float)y
{
    
    
    
    OriginalFrame = fView.frame;
    superView = fView.superview;
    
    
    opacityLayer = [CALayer layer];
    opacityLayer.backgroundColor = [UIColor blackColor].CGColor;
    opacityLayer.frame = CGRectMake(0.0, 0.0,320, 480);
    opacityLayer.opacity  = 0.0;
    opacityLayer.transform = CATransform3DScale(CATransform3DMakeTranslation(0.0,0.0,-200),2,2,1);
    [self.view.layer insertSublayer:opacityLayer atIndex:0];
    
    
    fromView = fView;
    fromViewSize = fView.frame.size;
    startX = x;
    startY = y;
    fromView.frame = CGRectMake(x, y, fView.frame.size.width, fView.frame.size.height);
    NSLog(@"%@",NSStringFromCGRect(fromView.frame));
    cerr = fView.center;
    toViewSize = tView.frame.size;
    self.toView = tView;
    //toView.frame = fromView.frame;
    fromView.center = self.view.center;
    toView.center = fromView.center;
    
    scaleX = toViewSize.width/fromViewSize.width;
    scaleY = toViewSize.height/fromViewSize.height;

    //toView.backgroundColor = [UIColor redColor];
   // fromView.backgroundColor = [UIColor greenColor];
 
    [self.view addSubview:toView];
    [self.view addSubview:fromView];
    
    
    
    translationX =  (320 - toViewSize.width)/2.0 - startX + (scaleX - 1) * (fromViewSize.width/2.0);
    translationY =  (480 - toViewSize.height)/2.0 - startY+ (scaleY - 1) * (fromViewSize.height/2.0);
    
    
    
    // [self.view.layer addSublayer:fromView.layer];
}



- (CAAnimation *)getOpacityAnimation:(CGFloat)fromOpacity
                           toOpacity:(CGFloat)toOpacity
{
    CABasicAnimation *pulseAnimationx = [CABasicAnimation animationWithKeyPath:@"opacity"];
    pulseAnimationx.duration = 1.0;
    pulseAnimationx.fromValue = [NSNumber numberWithFloat:fromOpacity];
    pulseAnimationx.toValue = [NSNumber numberWithFloat:toOpacity];
    
    pulseAnimationx.autoreverses = NO;
    pulseAnimationx.fillMode=kCAFillModeForwards;
    pulseAnimationx.removedOnCompletion = NO;
    pulseAnimationx.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    return pulseAnimationx;
    
}

- (CAAnimation *)getAnimation:(float)fromScaleX
                     toScaleX:(float)toScaleX
                   fromScaleY:(float)fromScaleY
                 tofromScaleY:(float)toScaleY
             fromTranslationX:(float)fromTranslationX
               toTranslationX:(float)toTranslationX
             fromTranslationY:(float)fromTranslationY
               toTranslationY:(float)toTranslationY
             fromTranslationZ:(float)fromTranslationZ
               toTranslationZ:(float)toTranslationZ
                 fromRotation:(float)fromRotation
                   toRotation:(float)toRotation
          removedOnCompletion:(BOOL)isRemove
{
    
    CAAnimationGroup *anim;
    
    CABasicAnimation *pulseAnimationx = [CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
    //  CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.z"];
    pulseAnimationx.duration = 1.0;
    pulseAnimationx.fromValue = [NSNumber numberWithFloat:fromScaleX];
    pulseAnimationx.toValue = [NSNumber numberWithFloat:toScaleX];
    
    CABasicAnimation *pulseAnimationy = [CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    //  CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.z"];
    pulseAnimationy.duration = 1.0;
    pulseAnimationy.fromValue = [NSNumber numberWithFloat:fromScaleY];
    pulseAnimationy.toValue = [NSNumber numberWithFloat:toScaleY];
    
    CABasicAnimation *translationx = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    translationx.duration = 1.0;
    translationx.fromValue = [NSNumber numberWithFloat:fromTranslationX];
    translationx.toValue = [NSNumber numberWithFloat:toTranslationX];
    
    CABasicAnimation *translationy = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    translationy.duration = 1.0;
    translationy.fromValue = [NSNumber numberWithFloat:fromTranslationY];
    translationy.toValue = [NSNumber numberWithFloat:toTranslationY];
    
    CABasicAnimation *pulseAnimationz = [CABasicAnimation animationWithKeyPath:@"transform.translation.z"];
    pulseAnimationz.duration = 1.0;
    pulseAnimationz.beginTime = 0.5;
    pulseAnimationz.fromValue = [NSNumber numberWithFloat:fromTranslationZ];
    pulseAnimationz.toValue = [NSNumber numberWithFloat:toTranslationZ];
    
    
    CABasicAnimation *rotateLayerAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotateLayerAnimation.duration = 1.0;
    rotateLayerAnimation.beginTime = 0.0;
    rotateLayerAnimation.fillMode = kCAFillModeBoth;
    rotateLayerAnimation.fromValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(fromRotation)];
    rotateLayerAnimation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(toRotation)];
    
    anim = [CAAnimationGroup animation];
    anim.animations = [NSArray arrayWithObjects:pulseAnimationx,pulseAnimationy,translationx,translationy,pulseAnimationz, rotateLayerAnimation, nil];
    anim.duration = 1.0;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    anim.autoreverses = NO;
    anim.fillMode=kCAFillModeForwards;
    anim.removedOnCompletion = isRemove;
    anim.delegate = self;
    //[self.view bringSubviewToFront:faceView];
    return anim;
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag&&animationSyte == 0)
    {
        toView.layer.transform = CATransform3DIdentity;
        fromView.layer.transform = CATransform3DIdentity;
        toView.center = CGPointMake(160,240);
        fromView.center = CGPointMake(160,240);
        [self.view bringSubviewToFront:toView];
        NSLog(@"animationDidStop:: %@,%@",NSStringFromCGRect(toView.frame),NSStringFromCGRect(toView.bounds));
        animationSyte = 1;
        
    }
    else if(flag&&animationSyte == 2)
    {
        [self dismiss];
        fromView.frame = OriginalFrame;
        [superView addSubview:fromView];
        
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
