//
//  MyDealsViewController.m
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "MyDealsViewController.h"
#import "AppDelegate.h"
#import "ProductCell.h"
#import "Product.h"
#import "ProductDetailViewController.h"

#define ProductCellHeight 64.0;

@interface MyDealsViewController (){
    AppDelegate *appDelegate;
}

@end

@implementation MyDealsViewController
@synthesize tblView;
@synthesize productsList;
@synthesize noProductsFoundLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        productsList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = @"My Deals";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    }
    
    
    [self fetchMyFavoritesAndUpdateList];
}

-(void)viewWillAppear:(BOOL)animated
{
   [self.navigationController.navigationBar setTranslucent:NO];
}
- (void)fetchMyFavoritesAndUpdateList {
    
    if (productsList.count > 0) {
        [productsList removeAllObjects];
    }
    
    [productsList addObjectsFromArray:[appDelegate myFavoriteProducts]];
    
    // If No products, hide table
    if (productsList.count == 0) {
        [self noProductsFound:YES];
    }
    else {
        [self noProductsFound:NO];
    }
    
    [self.tblView reloadData];
}


- (void) noProductsFound:(BOOL) notFound {
    
    if (notFound) {
        self.tblView.hidden = YES;
        self.noProductsFoundLabel.text = @"No Favourites Added";
        self.noProductsFoundLabel.hidden = NO;
        self.noProductsFoundLabel.layer.cornerRadius = 7.5;
        self.noProductsFoundLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        self.noProductsFoundLabel.layer.borderWidth = 2.0;
    }
    else {
        self.tblView.hidden = NO;
        self.noProductsFoundLabel.hidden = YES;
    }
    
}
//=============================
#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    NSLog(@"tableView numberOfRowsInSection");
    return [productsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";

    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    
    Product *aPro = [productsList  objectAtIndex:indexPath.row];
    
    // Set title and offer text here
    cell.productNameLabel.text = aPro.productName;
    cell.productOfferLabel.text = aPro.productOffer;
    cell.imgLoadingIndicator.hidesWhenStopped = YES;
    
    
    // Set different background colors for the cells.
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0) {
        //cell.headerContainerView.backgroundColor = [UIColor colorWithRed:244/255.0 green:123/255.0 blue:75/255.0 alpha:1.0];
        cell.productNameLabel.textColor=[UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
    }
    else if (rowModuleNo == 1) {
        //cell.headerContainerView.backgroundColor = [UIColor colorWithRed:253/255.0 green:225/255.0 blue:206/255.0 alpha:1.0];
        cell.productNameLabel.textColor=[UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ProductCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    Product *aPro = [productsList objectAtIndex:indexPath.row];
    ProductDetailViewController *myref = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil productID:aPro.productId];
    
    [self.navigationController pushViewController:myref animated:YES];
  /*
    ProductDetailViewController *myref = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil productID:aPro.productId];
    
    [self.navigationController pushViewController:myref animated:YES];  */
}
//================================================================

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
