//
//  ShareAppViewController.h
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ShareAppViewController : UIViewController<MFMailComposeViewControllerDelegate>

- (IBAction)facebookButon_Tapped:(id)sender;
- (IBAction)twitterButton_Tapped:(id)sender;
- (IBAction)emailButton_Tapped:(id)sender;
@end
