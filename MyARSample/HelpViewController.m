//
//  HelpViewController.m
//  GrabItNow
//
//  Created by Venkat Sasi Allamraju on 05/03/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "HelpViewController.h"
#import "DMLazyScrollView.h"
#import "AppDelegate.h"

@interface HelpViewController ()<DMLazyScrollViewDelegate>{
    
    AppDelegate *appDelegate;
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    NSArray *pageImages_Array;
    NSArray *pageContent_Array;
    UIWebView   *helpWebView;
    NSString *htmlString;
}
@property(nonatomic,retain)NSArray *pageContent_Array;
@property(nonatomic,strong)NSArray *pageImages_Array;

- (void) showActivityView;
- (void) dismissActivityView;

@end//hImage41

@implementation HelpViewController
@synthesize pageImages_Array;
@synthesize pageContent_Array;
@synthesize coarouselPage_Control;
@synthesize screens_ImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController setNavigationBarHidden:NO];
//    [appDelegate.homeViewController setHeaderTitle:@"Help"];
//    [appDelegate.homeViewController hideBackButon:YES];
      [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    self.title = @"Help";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    }

    
    if(![appDelegate isIphone5])
    {
        NSLog(@"Iphone4...");
        CGRect Frame = self.screens_ImageView.frame;
        Frame.origin.x = 0;
        //  Frame.size.height = Frame.size.height-150;
        self.screens_ImageView.frame = Frame;
        CGRect Frame1 = self.coarouselPage_Control.frame;
        NSLog(@"position is %f ",Frame1.origin.y);
        Frame1.origin.y=338;
        self.coarouselPage_Control.frame = Frame1;
        
        
    }
    
    //configuring pages &nbsp&nbsp&nbsp&nbsp
    NSString *page1 =@"<h4>   Welcome to your Rewards program... &nbsp&nbsp </h4> Enjoy ongoing, uninterrupted acess to thousands of discounts on your phone. <br><br>&#9733; You can search by category, by keyword <br>&nbsp&nbsp&nbsp&nbsp and also search local with \"What around <br>&nbsp&nbsp&nbsp&nbsp me\".<br><br>&#9733; Select a merchant offer you like and <br>&nbsp&nbsp&nbsp&nbsp present the coupon on your phone at the <br>&nbsp&nbsp&nbsp&nbsp time of settling your bill. Always check the <br>&nbsp&nbsp&nbsp&nbsp terms of the offer.<br><br>&#9733; Some merchants only offer their services <br>&nbsp&nbsp&nbsp&nbsp online and in most cases should be able <br>&nbsp&nbsp&nbsp&nbsp to click through to their online offerings.<br><br><br><br><br><br><br><br><br><br><br><br>";
    //&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    NSString *page2 = @"<h4>  Searching  for discounts is EASY! </h4> &#9733; Go to <b>Search</b> by category  in the menu... <br><br>&#9733;  Select an <b>offer category</b> that interests <br>&nbsp&nbsp&nbsp&nbsp you... <br><br>&#9733;  Include the <b>suburb or city</b> you  are <br>&nbsp&nbsp&nbsp&nbsp searching... <br><br>&#9733;  Add a <b>keyword</b> to help refine your  <br>&nbsp&nbsp&nbsp&nbsp  search... <br><br><br><br><br><br><br><br><br>";
    
    //&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    NSString *page3 = @"<h4>  What's Around Me! </h4> This local search function show offers that are around you wherever you are. <br><br>&#9733; You can expand or reduce the search area &nbsp&nbsp&nbsp&nbsp on screen.<br><br>&#9733;  The offer drop pins have an icon <br>&nbsp&nbsp&nbsp&nbsp representing type of offer.<br><br>&#9733; For example dining has a knife and fork <br>&nbsp&nbsp&nbsp&nbsp icon pin. <br><br>&#9733; The icon range are shown in the main <br>&nbsp&nbsp&nbsp&nbsp search function. <br><br><br><br><br><br><br><br><br>";
    
    //&nbsp&nbsp&nbsp&nbsp
    NSString *page4 = @"<h4> Search Results wherever you are... </h4>  &#9733;Your search results will be retrieved.<br> <br>&#9733; If you don't get a result , widen your <br>&nbsp&nbsp&nbsp&nbsp search parameters.<br> <br>&#9733; You will receive a list of offers that <br>&nbsp&nbsp&nbsp&nbsp   meet the search criteria <br><br>&#9733; Click on your preferred supplier in the <br>&nbsp&nbsp&nbsp&nbsp  search results <br> <br>&#9733; This will take type the offer page <br>&nbsp&nbsp&nbsp&nbsp  of that merchant <br><br><br><br><br><br><br><br><br><br><br>";
    //&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    NSString *page5 = @"<h4>  Your selected offer...  </h4>&#9733; You will  now see the offer  details, <br> &nbsp&nbsp&nbsp&nbsp including the address, contact details of <br>&nbsp&nbsp&nbsp&nbsp the  merchant, and all terms and <br> &nbsp&nbsp&nbsp&nbsp conditions.<br> <br>&#9733;   If you like the offer add it to \"your <br>&nbsp&nbsp&nbsp&nbsp favourites\" by clicking the heart. <br><br><br><br><br><br><br><br><br><br><br>";
    //&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    NSString *page6 = @"<h4>  Your Coupon  </h4>&#9733; You now have the offer coupon on your <br> &nbsp&nbsp&nbsp&nbsp phone. How cool's that ! <br><br>&#9733;   Click the T&C button to see the terms and <br> &nbsp&nbsp&nbsp&nbsp conditions of use that apply to that <br> &nbsp&nbsp&nbsp&nbsp merchant.<br><br>&#9733; Read all offer terms and conditions <br> &nbsp&nbsp&nbsp&nbsp carefully before presentation to make sure <br> &nbsp&nbsp&nbsp&nbsp that you use the correct redemption option <br> &nbsp&nbsp&nbsp&nbsp as <b>some offers can only be redeemed <br> &nbsp&nbsp&nbsp&nbsp online</b>.<br><br>&#9733; Each coupon offer  has a <b> \"Merchant <br>&nbsp&nbsp&nbsp&nbsp Redeem \" </b> button. <b>THIS &nbsp&nbspIS FOR <br>&nbsp&nbsp&nbsp&nbsp MERCHANT USE ONLY </b>. Some offers <br>&nbsp&nbsp&nbsp&nbsp only allow a SINGLE USE, so don't waste <br>&nbsp&nbsp&nbsp&nbsp it by pressing the button. As once it's <br>&nbsp&nbsp&nbsp&nbsp gone... it's gone!<br><br> Happy saving... but if all else fails send an email to: <br> <a href=\"mailto:support@therewardsteam.com\">support@therewardsteam.com</a>.   <br><br><br><br><br><br><br><br><br><br>";
    
    // <a href="mailto:someone@example.com?Subject=Hello%20again" target="_top">
    self.pageContent_Array = [[NSArray alloc]initWithObjects:page1,page2,page3,page4,page5,page6,nil];
    
    NSLog(@" page array count ::%d",[self.pageContent_Array count]);
    
    // self.pageImages_Array = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"hImage1.png"],[UIImage imageNamed:@"hImage2.png"],[UIImage imageNamed:@"hImage3.png"],[UIImage imageNamed:@"hImage4.png"],[UIImage imageNamed:@"hImage5.png"],[UIImage imageNamed:@"hImage6.png"], nil];
    
    
    self.pageImages_Array = [[NSArray alloc]initWithObjects:@"hImage1@2x",@"hImage2@2x",@"hImage3@2x",@"hImage4@2x",@"hImage5@2x",@"hImage6@2x", nil];
    
    // PREPARE PAGES
    NSUInteger numberOfPages = 6;
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, 548.0f);
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:rect];
    
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
        return [self controllerAtIndex:index];
    };
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [self.view addSubview:lazyScrollView];
    
    
    [self.view bringSubviewToFront:self.coarouselPage_Control];
    
    // self.screens_ImageView.image = [self.pageImages_Array objectAtIndex:0];
    
    
}

- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
    //UIWebView   *helpWebView;
    
    
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor clearColor];
        
        
        if ([appDelegate isIphone5])
            helpWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320, 566.0f)];
        else
            helpWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320, 440.0f)];
        helpWebView.backgroundColor = [UIColor whiteColor];
        //if(index == 5)
//        helpWebView.delegate = self;
        NSString * helpText = [self.pageContent_Array objectAtIndex:index];
        
        
        helpText = [NSString stringWithFormat:@"<html> \n"
                    "<head> \n"
                    "<style type=\"text/css\"> \n"
                    "body {font-family: \"%@\"; font-size: %@; text-align: %@; font-color: \"%@\"}\n"
                    "</style> \n"
                    "</head> \n"
                    "<body>%@</body> \n"
                    "</html>", @"Helvetica", [NSNumber numberWithInt:15],@"left", @"#100F0F", helpText];
        
        
        NSString *imageName = [self.pageImages_Array objectAtIndex:index];
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
        NSLog(@"imagePath::%@",imagePath);
        
        htmlString = [NSString stringWithFormat:@"<html><body p style='color:#3BB9FF'><img src=\"file://%@\"><style type='text/css'>img { max-width: 304%; width: auto; height: auto; }</style>%@</body></html>",imagePath,helpText];
        
        
        // NSString *htmlString = [NSString stringWithFormat:@"<html><body p style='color:#000000' >%@</body></html>",  helpText];
        [helpWebView loadHTMLString:htmlString baseURL:nil];
        
        //[helpWebView setOpaque:NO];
        [contr.view addSubview:helpWebView];
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex
{
    NSLog(@"currentPage %d",currentPageIndex);
    
    
    self.coarouselPage_Control.currentPage = currentPageIndex;
    
    // self.screens_ImageView.image = [self.pageImages_Array objectAtIndex:currentPageIndex];
    
    
}


- (BOOL)webView:(UIWebView *)webView1 shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType{
    
    NSLog(@"shouldStartLoadWithRequest");
    
    NSURL *requestURL = [request URL];
    NSString *str_url = [requestURL absoluteString];
    NSLog(@"%@",str_url);
    //if([[[request URL] scheme] isEqual:@"mailto"]){
//    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
//        [appDelegate.homeViewController showMailComposer];}
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setCoarouselPage_Control:nil];
    [super viewDidUnload];
}
@end
