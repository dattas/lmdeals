//
//  PostADealMenuViewController.m
//  MyARSample
//
//  Created by vairat on 13/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "PostADealMenuViewController.h"
#import "PostADealViewController.h"
#import "ProductDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "SKBounceAnimation.h"
#import "Cell1.h"
#import "Cell2.h"

#define Status_XML_tag @"status"
#define Status_success_message @"SUCCESS"
#define Status_failure_message @"FAILURE"
#define Default_keyboard_height_iPhone 216.0

@interface PostADealMenuViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    
    UIView *loginProcessingView;
    NSURLConnection *loginRequest;
    NSURLConnection *userDetailsRequest;
    NSURLConnection *dealsConnection;
    
    NSString *userName;
    NSString *password;
    NSMutableArray *dealsList;
    NSMutableArray *futureDeals_Array;
    NSMutableArray *runningDeals_Array;
    NSMutableArray *expiredDeals_Array;
    
    
    
    NSXMLParser *parser;
    UserDataXMLParser *parserDelegate;
    ProductListParser *productsXMLParser;
    
    BOOL menuViewUp;
    AppDelegate *appDelegate;

}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,retain)IBOutlet UITableView *expansionTableView;
@end

@implementation PostADealMenuViewController

@synthesize loginContainer_Container;
@synthesize loginContainerView;
@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize submitButton;
@synthesize menuView,menuButton;
@synthesize isOpen,selectIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    menuViewUp=NO;
    self.title = @"Login";
    self.expansionTableView.sectionFooterHeight = 0;
    self.expansionTableView.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    futureDeals_Array = [[NSMutableArray alloc]init];
    runningDeals_Array = [[NSMutableArray alloc]init];
    expiredDeals_Array = [[NSMutableArray alloc]init];
    
    //[runningDeals_Array addObject:@"prashanth"];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu.png"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    }

    
    // ** Initial frame changes to the login container
    CGRect loginContainerFrame = loginContainerView.frame;
    loginContainerFrame.size.width = loginContainerFrame.size.width - 40.0;
    loginContainerView.frame = loginContainerFrame;
    loginContainerView.center = self.view.center;
    
    // ** Apply round corners to the Container
    loginContainerView.layer.cornerRadius = 10.0;
    //loginContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    loginContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    loginContainerView.layer.shadowOpacity = 1.0;
    loginContainerView.layer.shadowOffset = CGSizeMake(0.0, 10.0);
    loginContainerView.layer.shadowRadius = 10.0;
    
    CGRect userNameFrame = usernameTextField.frame;
    userNameFrame.origin.x = (loginContainerView.frame.size.width - userNameFrame.size.width)/2.0;
    usernameTextField.frame = userNameFrame;
    
    CGRect pWordFrame = passwordTextField.frame;
    pWordFrame.origin.x = (loginContainerView.frame.size.width - pWordFrame.size.width)/2.0;
    passwordTextField.frame = pWordFrame;
    
    
    CGRect submitBtnFrame = submitButton.frame;
    submitBtnFrame.origin.x = (loginContainerView.frame.size.width - submitBtnFrame.size.width)/2.0;
    submitButton.frame = submitBtnFrame;
    
    
    [self.view addSubview:self.loginContainer_Container];
}

-(void)viewWillAppear:(BOOL)animated
{
   [self.navigationController.navigationBar setTranslucent:NO];
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Bar1.png"] forBarMetrics:UIBarMetricsDefault];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
- (void) viewDidAppear:(BOOL)animated {
    
     NSLog(@"viewDidAppear...");
    [super viewDidAppear:animated];
    [self readjustloginContainer];
    
}

- (IBAction)postADealButtonTapped:(id)sender{
    
    PostADealViewController *postaDeal = [[PostADealViewController alloc]initWithNibName:@"PostADealViewController" bundle:Nil throughView:@"Menu"];
    
    [self.navigationController pushViewController:postaDeal animated:YES];
}




- (void) readjustloginContainer {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = loginContainerView.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height-15;;
        loginContainerView.frame = frame;
        //loginContainerView.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)usernameTextFieldDidBegin {
    [self bringTheLoginViewUp];
}
- (void)passwordTextFieldDidBegin {
    [self bringTheLoginViewUp];
}
- (void)passwordTextFieldDidClose {
    
    [passwordTextField resignFirstResponder];
    [self readjustloginContainer];
    
}


- (void) bringTheLoginViewUp {
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        float bottomMargin = 5.0;
        
        CGRect loginContainerFrame = loginContainerView.frame;
        loginContainerFrame.origin.y = self.view.frame.size.height - (loginContainerFrame.size.height + Default_keyboard_height_iPhone + bottomMargin);
        loginContainerView.frame = loginContainerFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void) dismissLoginContainerWithUserDetails:(User *)uDetails password:(NSString *) pWord {
    
    
    usernameTextField.hidden = passwordTextField.hidden  = submitButton.hidden = YES;
    
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        loginContainerView.layer.masksToBounds = YES;
        loginContainerView.frame = CGRectMake((self.view.frame.size.width/2.0 - 1), (self.view.frame.size.height/2.0 - 1), 2.0, 2.0);
    } completion:^(BOOL finished) {
        [loginContainerView removeFromSuperview];
        
        /*** These statements are called on successful login ***/
        AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
       // uDetails.clientDomainName = subDomainName;
        [appDelegate loginSuccessfulWithUserdetails:uDetails password:pWord];
        
    }];
    
    NSLog(@"dismissLoginContainerWithUserDetails");
    [self.loginContainer_Container removeFromSuperview];
    [self getDeals];

}

//====================
- (IBAction)loginButtonTapped:(id)sender
{
    //[self.loginContainer_Container removeFromSuperview];
     self.title = @"Menu";
    
    if ([usernameTextField isFirstResponder]) {
        [usernameTextField resignFirstResponder];
    }
    
    if ([passwordTextField isFirstResponder]) {
        [passwordTextField resignFirstResponder];
    }
 
     [self readjustloginContainer];
    
    if (![self validateLogin]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory. \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self performLoginWithUserName:usernameTextField.text password:passwordTextField.text];
    
        
}



-(void)getDeals{
    
    NSString *merchantID = @"13064";
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_deals.php?m_id=%@",URL_Prefix,merchantID]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    dealsConnection = [[NSURLConnection alloc] initWithRequest:request
                                                   delegate:self];
    
    
    
}

- (BOOL) validateLogin {
    
    BOOL success = YES;
    
    if (usernameTextField.text.length == 0 || passwordTextField.text.length == 0) {
        success = NO;
    }
    
    return success;
}
- (void) performLoginWithUserName:(NSString *) uName password:(NSString *) pWord{
   
   
    if ([usernameTextField isFirstResponder]) {
        [usernameTextField resignFirstResponder];
    }
    
    if ([passwordTextField isFirstResponder]) {
        [passwordTextField resignFirstResponder];
    }
    
    userName = uName;
    password = pWord;
  
    [self showLoginProgressingView];
    
    /**** Here goes actual login checking code ****/
    NSString *domainName = @"www.myrewardsatwork.in";

    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_login.php?uname=%@&pwd=%@&sub=%@",URL_Prefix,uName,pWord,domainName]];
    NSLog(@"URL is %@",aUrl);
    NSMutableURLRequest *arequest = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    loginRequest = [[NSURLConnection alloc] initWithRequest:arequest delegate:self];
    
      
}

- (void) showLoginProgressingView {
    CGSize processingViewSize = CGSizeMake(self.view.frame.size.width*(3/4.0), self.view.frame.size.height*(1/4.0));
    
    CGPoint processingViewPoint = CGPointMake( (self.view.frame.size.width - processingViewSize.width)/2, (self.view.frame.size.height - processingViewSize.height)/2);
    CGRect processingViewrect = CGRectZero;
    processingViewrect.origin = processingViewPoint;
    processingViewrect.size = processingViewSize;
    
    if (!loginProcessingView) {
        loginProcessingView = [[UIView alloc] initWithFrame:processingViewrect];
        loginProcessingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UILabel *loadingLabel = [[UILabel alloc] initWithFrame:loginProcessingView.bounds];
        loadingLabel.textColor = [UIColor whiteColor];
        loadingLabel.tag = 2020;
        loadingLabel.font = [UIFont systemFontOfSize:17.0];
        loadingLabel.textAlignment = UITextAlignmentCenter;
        loadingLabel.backgroundColor = [UIColor clearColor];
        loadingLabel.text = @"Processing ...";
        loadingLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        
        [loginProcessingView addSubview:loadingLabel];
        
        loginProcessingView.layer.cornerRadius = 5.0;
        loginProcessingView.layer.shadowColor = [UIColor colorWithWhite:0.3 alpha:1.0].CGColor;
        
        
    }
    
    loginProcessingView.frame = processingViewrect;
    [self.view addSubview:loginProcessingView];
}

- (void) removeLoginProgressView {
    
    [loginProcessingView removeFromSuperview];
}

- (void) getuserDetails {
    
    NSLog(@"getuserDetails..");
    
    if (loginProcessingView) {
        UILabel *messLabel = (UILabel *)[loginProcessingView viewWithTag:2020];
        messLabel.text = @"Loading ...";
    }
    
    //need to send user ID here as parameter.
   
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user.php?uname=%@",URL_Prefix,userName]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    userDetailsRequest = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

- (IBAction)menuButton_Action:(id)sender
{
    
    
    if ([appDelegate isIphone5]) {
        
    
    if(menuViewUp)
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.menuView.frame;
            ContainerFrame.origin.y = self.menuView.frame.origin.y + 48.0;
            self.menuView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             menuViewUp = NO;
                             
                             [self.menuButton setImage:[UIImage imageNamed:@"bMenu.png"] forState:UIControlStateNormal];
                         } ];
        
    }
    else
    {
        
        
        NSString *keyPath = @"position.y";
        id finalValue = [NSNumber numberWithFloat:516 - 45.0];
        
        SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
        bounceAnimation.fromValue = [NSNumber numberWithFloat:516];
        bounceAnimation.toValue = finalValue;
        bounceAnimation.duration = 0.6f;
        bounceAnimation.numberOfBounces = 4;
        bounceAnimation.stiffness = SKBounceAnimationStiffnessLight;
        bounceAnimation.shouldOvershoot = YES;
        
        [self.menuView.layer addAnimation:bounceAnimation forKey:@"someKey"];
        
            
        [self.menuView.layer setValue:finalValue forKeyPath:keyPath];
        
        menuViewUp = YES;
        [self.menuButton setImage:[UIImage imageNamed:@"bClose.png"] forState:UIControlStateNormal];
        
    }
    }else
    {
        if(menuViewUp)
        {
            
            [UIView animateWithDuration:0.3 animations:^{
                
                CGRect ContainerFrame = self.menuView.frame;
                ContainerFrame.origin.y = self.menuView.frame.origin.y + 48.0;
                self.menuView.frame = ContainerFrame;}
             
                             completion:^(BOOL finished) {
                                 menuViewUp = NO;
                                 
                                 [self.menuButton setImage:[UIImage imageNamed:@"bMenu.png"] forState:UIControlStateNormal];
                             } ];
            
        }
        else
        {
            
            
            NSString *keyPath = @"position.y";
            id finalValue = [NSNumber numberWithFloat:self.view.frame.size.height-menuView.frame.size.height+90];
            
            SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
            bounceAnimation.fromValue = [NSNumber numberWithFloat:516];
            bounceAnimation.toValue = finalValue;
            bounceAnimation.duration = 0.6f;
            bounceAnimation.numberOfBounces = 4;
            bounceAnimation.stiffness = SKBounceAnimationStiffnessLight;
            bounceAnimation.shouldOvershoot = YES;
            
            [self.menuView.layer addAnimation:bounceAnimation forKey:@"someKey"];
            
            
            [self.menuView.layer setValue:finalValue forKeyPath:keyPath];
            
            menuViewUp = YES;
            [self.menuButton setImage:[UIImage imageNamed:@"bClose.png"] forState:UIControlStateNormal];
            
        }

    
    
    }
}
//=======================-----------UITABLEVIEW DELEGATE METHODS---------================================//

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen) {
        if (self.selectIndex.section == section) {
            if (self.selectIndex.section == 0) {
                return [runningDeals_Array count]+1;
            }
            else if (self.selectIndex.section == 1) {
                return [futureDeals_Array count]+1;
            }
            else
                return [expiredDeals_Array count]+1;
        }  
        
    }
    return 1;
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if((indexPath.section == 0 && indexPath.row == 0) || (indexPath.section == 1 && indexPath.row == 0) || (indexPath.section == 2 && indexPath.row == 0))
        return 50;
    else
        return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isOpen&&self.selectIndex.section == indexPath.section&&indexPath.row!=0) {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        
        
        Product *pro;
        if(indexPath.section == 0 )
                pro = [runningDeals_Array objectAtIndex:indexPath.row-1];
        else if (indexPath.section == 1 )
                pro = [futureDeals_Array objectAtIndex:indexPath.row-1];
        else if (indexPath.section == 2 )
                pro =  [expiredDeals_Array objectAtIndex:indexPath.row-1];
        
        cell.titleLabel.text = pro.productName;
        cell.discountLabel.text = pro.productOffer;
        return cell;
    }else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        //NSString *name = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
        
        switch (indexPath.section) {
            case 0:
                cell.titleLabel.text = @"Running Deals";
                break;
            case 1:
                cell.titleLabel.text = @"Future Deals";
                break;
            case 2:
                cell.titleLabel.text = @"Expired Deals";
                break;
                
            default:
                break;
        }
        
        
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Section::%d   Row::%d",indexPath.section,indexPath.row);
    if (indexPath.row == 0) {
        if ([indexPath isEqual:self.selectIndex]) {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            
        }else
        {
            if (!self.selectIndex) {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
            }else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }else  // click on rows
    {

        Product *pro;
        if (indexPath.section == 0) 
             pro  = [runningDeals_Array objectAtIndex:indexPath.row-1];
        else if (indexPath.section == 1)
            pro  = [futureDeals_Array objectAtIndex:indexPath.row-1];
        else
           pro  = [expiredDeals_Array objectAtIndex:indexPath.row-1];
        
        NSLog(@"productName::%@",pro.productName);
        
        ProductDetailViewController *myref = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil productID:pro.productId];
        
        [self.navigationController pushViewController:myref animated:YES];
        
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    self.isOpen = firstDoInsert;
    
    Cell1 *cell = (Cell1 *)[self.expansionTableView cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    [self.expansionTableView beginUpdates];
    
    int section = self.selectIndex.section;
    int limit;
    if (section == 0) 
        limit = [runningDeals_Array count];
    else  if (section == 1)
        limit = [futureDeals_Array count];
    else
         limit = [expiredDeals_Array count];
    
    
	NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	for (NSUInteger i = 1; i < limit+1; i++) {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {   [self.expansionTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [self.expansionTableView deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
	
	
	[self.expansionTableView endUpdates];
    if (nextDoInsert) {
        self.isOpen = YES;
        self.selectIndex = [self.expansionTableView indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [self.expansionTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}


//=======================-----------NSURLCONNECTION METHODS---------================================//

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"didReceiveResponse");
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [_responseData appendData:data];
    
    if(connection == loginRequest)
    {
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"loginRequest...%@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:_responseData];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        
    }
    else if(connection == dealsConnection){
        
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"dealsConnection...%@",responseString);
        
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:_responseData];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        
        
    }
    else
    {
        NSLog(@"UserDetails Request...");
        NSString *responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
        NSLog(@"UserDetails::%@",responseString);
        NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:_responseData];
        parserDelegate = [[UserDataXMLParser alloc] init];
        parserDelegate.delegate = self;
        userDetailsParser.delegate = parserDelegate;
        [userDetailsParser parse];
        
        
        [self removeLoginProgressView];
       
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConnection");
   
}
//=====================------------------ PARSING METHODS --------------=============================//
#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementString;



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"status"]) {
        parserElementString = [[NSMutableString alloc] init];
        NSLog(@"%@",parserElementString);
    }
    else if ([elementName isEqualToString:@"first_status"]) {
        parserElementString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"first_response"]) {
        parserElementString = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementString appendString:string];
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:Status_XML_tag]) {
        
        if ([parserElementString isEqualToString:Status_success_message])
        {
            
            [self getuserDetails];
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            [self removeLoginProgressView];
        }
        
    }
    else{
        NSLog(@"elementName::%@",elementName);
    }
    
        
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parserLocal {
    parser.delegate = nil;
    parser = nil;
}

- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError {
    parser.delegate = nil;
    parser = nil;
}

#pragma mark -- UserDataXMLParser Delegate methods

- (void)parsingUserDetailsFinished:(User *)userDetails {
    
    [self dismissLoginContainerWithUserDetails:userDetails password:password];
    
}

- (void)userDetailXMLparsingFailed {
    
}
#pragma mark -- ProductListXMLParser Delegate methods
- (void) parsingProductListFinished:(NSArray *) productsList{


    // Here update products list
    if (!dealsList)
    
        dealsList = [[NSMutableArray alloc] initWithArray:productsList];
    
    else
    
        [dealsList addObjectsFromArray:productsList];
    
   
    NSLog(@"Total dealsList Count::%d",[dealsList count]);
    
    
    //to seperete future,current and past deals basing on date
    NSDate *currentDate = [NSDate date];
    NSComparisonResult result;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
   // NSLog(@"currentDateStr%@",currentDateStr);
  //  NSLog(@"=============================");
    currentDate = [dateFormatter dateFromString:currentDateStr];
   
    for (int i = 0; i< [dealsList count]; i++) {
        
        Product *pro = [dealsList objectAtIndex:i];
        result = [[dateFormatter dateFromString:pro.product_StartDate] compare:currentDate];
        
        
        switch (result)
        {
            case NSOrderedAscending:  //to search end date
                                       //NSLog(@"NSOrderedAscending");
                                      result = [[dateFormatter dateFromString:pro.product_EndDate] compare:currentDate];
                                      switch (result)
                                       {
                                         case NSOrderedAscending:
                                            //  NSLog(@"Expired Deal Name: %@",pro.productName);
                                               [expiredDeals_Array addObject:[dealsList objectAtIndex:i]];
                                         break;
                                         case NSOrderedDescending:
                                             // NSLog(@"current Deal Name: %@",pro.productName);
                                               [runningDeals_Array addObject:[dealsList objectAtIndex:i]];
                                         break;
                        
                                         default:
                                         break;
                                        }
                
                          
                                      break;   
                
            case NSOrderedDescending:// NSLog(@"Future Deal Name %@",pro.productName);
                                      [futureDeals_Array addObject:[dealsList objectAtIndex:i]];
                                      break;
                
            case NSOrderedSame:      // NSLog(@"current Deal Name: %@",pro.productName);
                                      [runningDeals_Array addObject:[dealsList objectAtIndex:i]];
                                      break;
                
            default:                  NSLog(@"erorr dates "); break;
        }
       // NSLog(@"Start Date :: %@", pro.product_StartDate);
       // NSLog(@"End Date :: %@", pro.product_EndDate);
        NSLog(@"=============================");
        
    }
     [self.expansionTableView reloadData];
    NSLog(@"runningDeals_Array::%d",[runningDeals_Array count]);
    NSLog(@"futureDeals_Array::%d",[futureDeals_Array count]);
    NSLog(@"expiredDeals_Array::%d",[expiredDeals_Array count]);
    
    Product *apro;
    for (int i =1; i < [runningDeals_Array count]; i++) {
        apro = [runningDeals_Array objectAtIndex:i];
        NSLog(@"%@",apro.productName);
    }
    
}
- (void) parsingProductListXMLFailed{}

//============================= UITEXTFIELD DELEGATE METHODS ====================================//

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField) {
        [self passwordTextFieldDidClose];
    }
    return YES;

}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
   
    
    if (textField == usernameTextField) {
        [self usernameTextFieldDidBegin];
    }
    else if (textField == passwordTextField) {
        [self passwordTextFieldDidBegin];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
