//
//  PostADealViewController.h
//  MyARSample
//
//  Created by vairat on 10/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "MerchantListParser.h"
#import "Merchant.h"
#import "NSString_stripHtml.h"

@interface PostADealViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIActionSheetDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, MerchantXMLParserDelegate> {
    
    NSMutableArray *merchantList;
     CLLocationCoordinate2D Location;
    NSMutableData *_responseData;
    
     Product *current_PostingProduct;
}
@property(nonatomic,readwrite)CLLocationCoordinate2D Location;
@property(nonatomic,strong)UITextView *current_TextView;
@property(nonatomic,strong)IBOutlet UITextView *productName_TextView;
@property(nonatomic,strong)IBOutlet UITextView *productOffer_TextView;
@property(nonatomic,strong)IBOutlet UITableView *merchantTableView;
@property(nonatomic,strong)IBOutlet UILabel *endDate_Label;
@property(nonatomic,strong)IBOutlet UILabel *startDate_Label;
@property(nonatomic,strong)IBOutlet UILabel *hoursCountDown_Label;
@property(nonatomic,strong)IBOutlet UILabel *productLeftCount_Label;
@property(nonatomic,strong)IBOutlet UILabel *productCategory_Label;

@property(nonatomic,strong)IBOutlet UIImageView *product_ImageView;
@property(nonatomic,strong)IBOutlet UISegmentedControl *segmentedControl;

@property(nonatomic,strong)IBOutlet UIView *mainContentHolder;
@property(nonatomic,strong)IBOutlet UIView *addressAdd_View;
@property(nonatomic,strong)IBOutlet UIView *accessoryView;

@property (nonatomic, retain)UIPickerView *productsLeft_PickerView;
@property (nonatomic, retain)UIPickerView *hoursCountDown_PickerView;
@property (nonatomic, retain)Product *current_PostingProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil throughView:(NSString *)through;
-(IBAction)doneButton_Pressed:(id)sender;
-(IBAction)gridButton_Pressed:(id)sender;
-(IBAction)cameraButton_Pressed:(id)sender;
-(IBAction)photoGalleryButton_Pressed:(id)sender;
-(IBAction) segmentedControlIndexChanged: (id)sender;
@end
