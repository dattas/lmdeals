//
//  PostADealNextViewController.h
//  MyARSample
//
//  Created by vairat on 18/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchCell.h"
#import "Product.h"
#import "FlipViewController.h"

@interface PostADealNextViewController : UIViewController< UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSURLConnectionDelegate, FlipViewDelegate, UITextViewDelegate>{
    
    FlipViewController *flipViewController;
    Product *curr_PostingProduct;
    
    NSMutableData *_responseData;
}

@property(nonatomic,strong)IBOutlet UITableView *myTableView;
@property(nonatomic, strong)UITextField *myTextField;
@property(nonatomic, strong)UITextField *contactTextField;
@property(nonatomic, strong)UITextField *currentTextField;
@property(nonatomic,strong)IBOutlet UIView *accessoryView;

@property(nonatomic,retain)Product *curr_PostingProduct;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil postProduct:(Product *)pro throughView:(NSString *)through;
@end
