//
//  ViewController.h
//  MyARSample
//
//  Created by vairat on 26/06/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import "MetaioSDKViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
#import "ProductListParser.h"
#import "Product.h"



namespace metaio
{
    class IGeometry;   // forward declaration
}

@interface ViewController : MetaioSDKViewController <CLLocationManagerDelegate, ProductListXMLParserDelegate, NSURLConnectionDelegate>
{
    metaio::IBillboardGroup*   billboardGroup;   //!< Our default billboard group
  
    metaio::IRadar* m_radar;
    
     
    metaio::IGeometry* myBillboard;
    UIImage* myImage;
     NSMutableData *_responseData;
    
    bool locUpdate;
}
//@property (nonatomic, retain) CLLocation* currentLocation;				//!< Contains the current location
@property (nonatomic, strong) Product *prodDetail;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentLocation;
@property (nonatomic, retain) CLLocationManager *locationManager;
@end
