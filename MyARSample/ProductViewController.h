//
//  ProductViewController.h
//  MyARSample
//
//  Created by vairat on 03/07/13.
//  Copyright (c) 2013 vairat. All rights reserved.
// CURRENT

#import <UIKit/UIKit.h>
#import "Product.h"
#import "ProductDataParser.h"
#import "MerchantListParser.h"
#import "Merchant.h"
#import "NSString_stripHtml.h"

@interface ProductViewController : UIViewController<ProductXMLParserDelegate,UIWebViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, MerchantXMLParserDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSMutableData *_responseData;
    int phnoCount;
    NSMutableArray *phArray;
    NSMutableArray *merchantList;
    CLLocationCoordinate2D Location;
    
}

@property (nonatomic, strong) IBOutlet UIImageView *productImageView;
@property(nonatomic,readwrite)CLLocationCoordinate2D Location;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productID:(NSString *)ProId ;
- (IBAction)dissmissAction:(id)sender;

//==================
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic,strong) IBOutlet UIView * imgContainerView;
@property (nonatomic,strong) IBOutlet UIScrollView * scroller;
@property (nonatomic,strong) IBOutlet UIImageView * imageView;
@property (nonatomic,strong) IBOutlet UIImageView * couponBgImageView;
@property (nonatomic,strong) IBOutlet UIButton *favButton;
@property (nonatomic,strong) IBOutlet UIButton *redeemButton;
@property (nonatomic,strong) IBOutlet UIButton *frontRedeemButton;
@property (nonatomic,strong) IBOutlet UIButton *couponButton;

@property (nonatomic,strong) IBOutlet UIView * mainContentHolder;
@property (nonatomic,strong) IBOutlet UIButton *offerButton;
@property (nonatomic,strong) IBOutlet UIButton *contactButton;
@property (nonatomic,strong) IBOutlet UIButton *addressButton;

@property (nonatomic,strong) IBOutlet UILabel *noImageLabel;
@property (nonatomic, strong) IBOutlet UIView *couponView;
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *clientLabel;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *productOfferLabel;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *imageActivityIndicator;

@property (nonatomic, strong) UITableView *merchantTableView;
@property (nonatomic, strong) UIView *mailComposeView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *redeemActivityIndicator;
// Coupon views
@property (nonatomic,strong) IBOutlet UIImageView * couponMerchantImage;
@property (nonatomic,strong) IBOutlet UIImageView * couponImageView;
@property (nonatomic, strong) IBOutlet UILabel *TandCLabel;

@property (nonatomic, strong) IBOutlet UIButton *TandCButton;
@property (nonatomic, strong) IBOutlet UIView *TandCView;
@property (nonatomic, strong) IBOutlet UIView *couponFrontView;
@property (nonatomic, strong) IBOutlet UIView *couponBackView;
@property (nonatomic, strong) IBOutlet UITextView *couponTandCTextView;
@property (nonatomic, strong) IBOutlet UIButton *TandCBackButton;
@property (nonatomic, strong) IBOutlet UIView *couponFlipView;
@property (nonatomic, strong) UIImage *myCouponImage;
@property (nonatomic,strong) IBOutlet UIImageView * couponCardBg_ImageView;
@property (nonatomic,strong) IBOutlet UIImageView * couponCardBackBg_ImageView;

-(IBAction)handleCouponTap:(id)sender;
- (IBAction) termsAndCondButtonPressed:(id)sender;
- (IBAction) termsAndCondBackButtonPressed:(id)sender;

- (IBAction) favoritesButtonPressed:(id) sender;
- (IBAction) redeemButtonPressed:(id) sender;
- (IBAction) couponButtonPressed:(id) sender;

- (IBAction) offerButtonPressed:(id) sender;
- (IBAction) contactButtonPressed:(id) sender;
- (IBAction) addressButtonPressed:(id) sender;
- (IBAction) mapButtonPressed:(id) sender;

- (void) updateUIWithProductDetails:(Product *) aProduct;

@end





